"""
This script processes the emotion dataset Semaine
"""
__author__ = 'aiham'

import glob
import logging
import audio_processing
import random
import os
import numpy as np
from scikits import audiolab
import scipy.io.wavfile as wav
import bob
from enum import Enum
from configuration import Config
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

class emotionCodesSemaine(Enum):
	Happy = 1
	Relaxed = 2
	Neutral = 3
	Sad = 4
	Angry = 5


def processAudioDataset():

	# Configure logging
	logging_path = Config.working_dir + 'debug.log'
	logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

	logging.info('Starting normalizing speech files...')
	print('Starting normalizing speech files...')
	for subdir, dirs, files in os.walk(Config.speech_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			if w.find('Audio')!=-1:
				speech_file = subdir+'/'+w
				mixfile= subdir+'/'+'mix.wav'
				f = audiolab.sndfile(speech_file)
				sig = f.read_frames(f.get_nframes(), dtype=np.float64)
				f.close()  # use contextlib.closing in real code
				(rate,u)= wav.read(speech_file)
				signal = sig[:,0]/4+sig[:,1]/4+sig[:,2]/4+sig[:,3]/4
				f = audiolab.sndfile(mixfile, 'write', audiolab.formatinfo('wav', 'pcm16'), 1, rate)
				f.write_frames(signal)
				f.close()
				character = w.replace('.wav','').split('Audio_')[1]
				speech_file=subdir+'_'+character+'.wav'
				result_file = speech_file.replace(Config.speech_dir, Config.normalize_dir)
				speech_file=mixfile
				logging.debug('speech_file='+speech_file)
				print('speech_file='+speech_file)
				logging.debug('result_file='+result_file)
				print('result_file='+result_file)
				audio_processing.normalize_vs_noise(speech_file, Config.ref_noise, result_file)
	logging.info('Finished normalizing speech files!')
	print('Finished normalizing speech files!')

	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting transformation of 64bit float noise files to 16bit int...')
	print('Starting transformation of 64bit float noise files to 16bit int...')
	# Transform 64bit float noise files to 16bit int
	for subdir, dirs, files in os.walk(Config.noise_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			float_wav_file = subdir+w
			int_wav_file = float_wav_file.replace(Config.noise_dir, Config.noise_int_dir)
			logging.debug('float_wav_file ='+float_wav_file)
			print('float_wav_file ='+float_wav_file)
			logging.debug('int_wav_file='+int_wav_file)
			print('int_wav_file='+int_wav_file)
			audio_processing.float2int(float_wav_file, int_wav_file)
	logging.info('Finished transformation of data type!')
	print('Finished transformation of data type!')

	logging.info('Starting transformation of 64bit float ir files to 16bit int...')
	print('Starting transformation of 64bit float ir files to 16bit int...')
	# Transform 64bit float ir files to 16bit int
	for subdir, dirs, files in os.walk(Config.ir_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			float_wav_file = subdir+w
			int_wav_file = float_wav_file.replace(Config.ir_dir, Config.ir_int_dir)
			logging.debug('float_wav_file ='+float_wav_file)
			print('float_wav_file ='+float_wav_file)
			logging.debug('int_wav_file='+int_wav_file)
			print('int_wav_file='+int_wav_file)
			audio_processing.float2int(float_wav_file, int_wav_file)
	logging.info('Finished transformation of data type!')
	print('Finished transformation of data type!')

	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting mixing speech files with random noise...')
	print('Starting mixing speech files with random noise...')
	# Mix speech files with random noise
	for subdir, dirs, files in os.walk(Config.normalize_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
			for w in wavs:
				speech_file = subdir+'/'+w
				result_file = speech_file.replace(Config.normalize_dir, Config.noised_dir)
				# pick random noise file, must be 16bit int, 64bit int doesn't work!
				noise_candidates = audio_processing.filtr(os.listdir(Config.noise_int_dir),'.wav')
				noise_file = Config.noise_int_dir+random.choice(noise_candidates)
				logging.debug('speech_file='+speech_file)
				print('speech_file='+speech_file)
				logging.debug('noise_file ='+noise_file)
				print('noise_file ='+noise_file)
				logging.debug('result_file='+result_file)
				print('result_file='+result_file)
				executor.submit(audio_processing.add_noise,speech_file, noise_file, result_file)
	logging.info('Finished mixing speech files!')
	print('Finished mixing speech files!')

	if (Config.ir_name != 'none'):
		logging.info('')
		print('')
		logging.info('')
		print('')
		logging.info('Starting convoluting noised speech with random IR...')
		print('Starting convoluting noised speech with random IR...')
		# Convolute noised speech files with random impulse reaction
		for subdir, dirs, files in os.walk(Config.noised_dir):
			# iterate over all wavs in the subdirectories
			wavs = audio_processing.filtr(files, 'wav')
			# parallelize
			with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
				for w in wavs:
					if (w.find(Config.test_speaker) == -1):
						executor.submit(execConvolution, subdir,w)
		logging.info('Finished convoluting speech!')
		print('Finished convoluting speech!')

		convolveTestSpeaker()

def execConvolution(subdir, w):
	speech_file = subdir+'/'+w
	# pick random impulse reaction file
	ir_file = Config.ir_int_dir+random.choice(audio_processing.filtr(os.listdir(Config.ir_int_dir),'.wav'))

	# making sure the chosen ir is different from testing ir
	while (ir_file.find(Config.test_pos)!=-1 or ir_file.find(Config.test_ir) != -1):
		ir_file = Config.ir_int_dir+random.choice(audio_processing.filtr(os.listdir(Config.ir_int_dir),'.wav'))

	result_file_path = speech_file.replace(Config.noised_dir, Config.convolute_dir)
	result_ir_filename = os.path.splitext(os.path.basename(result_file_path))[0] + \
					 '_' + os.path.splitext(os.path.basename(ir_file))[0] + '.wav'
	path,fname = os.path.split(result_file_path)
	result_file = path + '/' + result_ir_filename
	logging.debug('speech_file='+speech_file)
	print('speech_file='+speech_file)
	logging.debug('ir_file    ='+ir_file)
	print('ir_file    ='+ir_file)
	logging.debug('result_file='+result_file)
	print('result_file='+result_file)
	audio_processing.convolute_ir(speech_file, ir_file, result_file)

def createCleanLearningDataset():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	type='clean'
	data_names = []
	data_anos = []
	data_mfccs = []
	for subdir, dirs, files in os.walk(Config.noised_dir):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			try:
				filedata=[]
				# Calculate MFCCS for file
				frame_mfccs = audio_processing.process_speech(Config.noised_dir+file)
				# Append all data to array with filename as identifier
				name = file.replace('.wav', '').split('_')[0]

				valencefile = audio_processing.filtr(os.listdir(Config.speech_dir+name+'/'),'DV.txt')
				anotationsv=[]
				for valence in valencefile:
					anotationsv.append(audio_processing.iter_load_txt_col(Config.speech_dir+name+'/'+valence,1)[:len(frame_mfccs)-250])


				arousalfile = audio_processing.filtr(os.listdir(Config.speech_dir+name+'/'),'DA.txt')
				anotationsa=[]
				for arousal in arousalfile:
					anotationsa.append(audio_processing.iter_load_txt_col(Config.speech_dir+name+'/'+arousal,1)[:len(frame_mfccs)-250])

				if len(valencefile)>1 and len(arousalfile)>1:
					meanvalence= np.divide(np.sum(anotationsv,axis=0),len(valencefile))
					meanarousal= np.divide(np.sum(anotationsa,axis=0),len(arousalfile))
					emotion = getSemaineEmotion(meanvalence,meanarousal)

					for i in range(min(len(frame_mfccs),len(meanarousal),len(meanvalence))):
						data_anos.append([meanvalence[i],meanarousal[i],emotion[i]])
						data_mfccs.append(frame_mfccs[i,:])
						data_names.append(int(name))
						temparray=np.ndarray.tolist(frame_mfccs[i,:])+[meanvalence[i]]+[meanarousal[i]]+[emotion[i]]
						filedata.append(temparray)

					features_output_file = Config.working_dir+'/'+type + 'Features/' + file.replace('.wav', '') +'.csv'
					logging.debug('clean features files: '+features_output_file)
					print('clean features files: '+features_output_file)
					bob.io.save(np.asmatrix(filedata), features_output_file, create_directories=True)
			except:
				pass


	print('Saved speech MFCCS!')
	logging.info('Saved speech MFCCS!')


def createChanneledLearningDataset():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	type='channeled'
	data_names = []
	data_anos = []
	data_mfccs = []
	for subdir, dirs, files in os.walk(Config.convolute_dir):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			try:
				filedata=[]
				# Calculate MFCCS for file
				frame_mfccs = audio_processing.process_speech(Config.convolute_dir+file)
				# Append all data to array with filename as identifier
				name = file.replace('.wav', '').split('_')[0]

				valencefile = audio_processing.filtr(os.listdir(Config.speech_dir+name+'/'),'DV.txt')
				anotationsv=[]
				for valence in valencefile:
					anotationsv.append(audio_processing.iter_load_txt_col(Config.speech_dir+name+'/'+valence,1)[:len(frame_mfccs)-250])

				arousalfile = audio_processing.filtr(os.listdir(Config.speech_dir+name+'/'),'DA.txt')
				anotationsa=[]
				for arousal in arousalfile:
					anotationsa.append(audio_processing.iter_load_txt_col(Config.speech_dir+name+'/'+arousal,1)[:len(frame_mfccs)-250])

				if len(valencefile)>1 and len(arousalfile)>1:
					meanvalence= np.divide(np.sum(anotationsv,axis=0),len(valencefile))
					meanarousal= np.divide(np.sum(anotationsa,axis=0),len(arousalfile))
					emotion = getSemaineEmotion(meanvalence,meanarousal)

					for i in range(min(len(frame_mfccs),len(meanarousal),len(meanvalence))):
						data_anos.append([meanvalence[i],meanarousal[i],emotion[i]])
						data_mfccs.append(frame_mfccs[i,:])
						data_names.append(int(name))
						temparray=np.ndarray.tolist(frame_mfccs[i,:])+[meanvalence[i]]+[meanarousal[i]]+[emotion[i]]
						filedata.append(temparray)

					features_output_file = Config.working_dir+'/'+type + 'Features/' + file.replace('.wav', '') + '.csv'
					logging.debug('channeled features files: '+features_output_file)
					print('channeled features files: '+features_output_file)
					bob.io.save(np.asmatrix(filedata), features_output_file, create_directories=True)
			except:
				pass

	print('Saved speech MFCCS!')
	logging.info('Saved speech MFCCS!')

def getSemaineEmotion(valence, arousal):
	emotion=[]
	emo=1
	for i in range(min(len(valence),len(arousal))):
		if abs(valence[i])<0.15 and abs(arousal[i])<0.15:
			emo=emotionCodesSemaine.Neutral
		else:
			if valence[i]>0:
				if arousal[i]>0:
					emo=emotionCodesSemaine.Happy
				else:
					emo=emotionCodesSemaine.Relaxed
			else:
				if arousal[i]>0:
					emo=emotionCodesSemaine.Angry
				else:
					emo=emotionCodesSemaine.Sad
		if not isinstance( emo, ( int, long ) ):
			emo=emo.value
		emotion.append(emo)
	return emotion

def splitSessionsToEmotionFiles():

	print('Splitting clean sessions into smaller files..')
	logging.info('Splitting clean sessions into smaller files..')

	type = 'clean'
	cleanFeaturesPath = Config.noised_dir.replace(type+'Data',type+'Features')

	for subdir, dirs, files in os.walk(cleanFeaturesPath):
		# iterate over all wavs in the subdirectories
		featuresFiles = audio_processing.filtr(files, 'csv')
		for file in featuresFiles:
			if (file.find(Config.test_speaker) == -1):
				# read file data
				filedata = audio_processing.iter_load_csv(cleanFeaturesPath+file)
				emotions = filedata[:,Config.num_features+2]
				emotions = np.array(emotions.flatten(),'int8')

				# split frames according to their emotions
				happy_frames = filedata[emotions == 1][:,:Config.num_features]
				relaxed_frames = filedata[emotions == 2][:,:Config.num_features]
				neutral_frames = filedata[emotions == 3][:,:Config.num_features]
				sad_frames = filedata[emotions == 4][:,:Config.num_features]
				angry_frames = filedata[emotions == 5][:,:Config.num_features]

				# create partial files from the current session
				happy_filename = Config.working_dir+type + 'Features/' + 'happy/' +\
								 os.path.splitext(file)[0] +'_h'+ os.path.splitext(file)[1]
				relaxed_filename = Config.working_dir+type + 'Features/' + 'relaxed/' + \
								   os.path.splitext(file)[0] +'_r'+ os.path.splitext(file)[1]
				neutral_filename = Config.working_dir+type + 'Features/' + 'neutral/' +\
								   os.path.splitext(file)[0] +'_n'+ os.path.splitext(file)[1]
				sad_filename = Config.working_dir+type + 'Features/' + 'sad/' + \
							   os.path.splitext(file)[0] +'_s'+ os.path.splitext(file)[1]
				angry_filename = Config.working_dir+type + 'Features/' + 'angry/' + \
								 os.path.splitext(file)[0] +'_a'+ os.path.splitext(file)[1]

				if (len(happy_frames) > 0):
					bob.io.save(happy_frames, happy_filename, create_directories=True)
				if (len(relaxed_frames) > 0):
					bob.io.save(relaxed_frames, relaxed_filename, create_directories=True)
				if (len(neutral_frames) > 0):
					bob.io.save(neutral_frames, neutral_filename, create_directories=True)
				if (len(sad_frames) > 0):
					bob.io.save(sad_frames, sad_filename, create_directories=True)
				if (len(angry_frames) > 0):
					bob.io.save(angry_frames, angry_filename, create_directories=True)

				features_output_file = Config.working_dir+'/'+type + 'Features/' + file
				logging.debug('processed clean features file: '+features_output_file)
				print('processed clean features file: '+features_output_file)
			else:
				# read file data
				filedata = audio_processing.iter_load_csv(cleanFeaturesPath+file)
				emotions = filedata[:,Config.num_features+2]
				emotions = np.array(emotions.flatten(),'int8')

				# split frames according to their emotions
				happy_frames = filedata[emotions == 1][:,:Config.num_features]
				relaxed_frames = filedata[emotions == 2][:,:Config.num_features]
				neutral_frames = filedata[emotions == 3][:,:Config.num_features]
				sad_frames = filedata[emotions == 4][:,:Config.num_features]
				angry_frames = filedata[emotions == 5][:,:Config.num_features]

				# create partial files from the current session
				happy_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/happy/' +\
								 os.path.splitext(file)[0] +'_h'+ os.path.splitext(file)[1]
				relaxed_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/relaxed/' + \
								   os.path.splitext(file)[0] +'_r'+ os.path.splitext(file)[1]
				neutral_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/neutral/' +\
								   os.path.splitext(file)[0] +'_n'+ os.path.splitext(file)[1]
				sad_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/sad/' + \
							   os.path.splitext(file)[0] +'_s'+ os.path.splitext(file)[1]
				angry_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/angry/' + \
								 os.path.splitext(file)[0] +'_a'+ os.path.splitext(file)[1]

				if (len(happy_frames) > 0):
					bob.io.save(happy_frames, happy_filename, create_directories=True)
				if (len(relaxed_frames) > 0):
					bob.io.save(relaxed_frames, relaxed_filename, create_directories=True)
				if (len(neutral_frames) > 0):
					bob.io.save(neutral_frames, neutral_filename, create_directories=True)
				if (len(sad_frames) > 0):
					bob.io.save(sad_frames, sad_filename, create_directories=True)
				if (len(angry_frames) > 0):
					bob.io.save(angry_frames, angry_filename, create_directories=True)

				features_output_file = Config.working_dir+'/'+type + 'Features/' + file
				logging.debug('processed clean features file: '+features_output_file)
				print('processed clean features file: '+features_output_file)


	type = 'channeled'
	channeledFeaturesPath = Config.convolute_dir.replace(type+'Data',type+'Features')

	for subdir, dirs, files in os.walk(channeledFeaturesPath):
		# iterate over all wavs in the subdirectories
		featuresFiles = audio_processing.filtr(files, 'csv')
		for file in featuresFiles:
			if (file.find(Config.test_speaker) == -1):
				# read file data
				filedata = audio_processing.iter_load_csv(channeledFeaturesPath+file)
				emotions = filedata[:,Config.num_features+2]
				emotions = np.array(emotions.flatten(),'int8')

				# split frames according to their emotions
				happy_frames = filedata[emotions == 1][:,:Config.num_features]
				relaxed_frames = filedata[emotions == 2][:,:Config.num_features]
				neutral_frames = filedata[emotions == 3][:,:Config.num_features]
				sad_frames = filedata[emotions == 4][:,:Config.num_features]
				angry_frames = filedata[emotions == 5][:,:Config.num_features]

				# create partial files from the current session
				happy_filename = Config.working_dir+type + 'Features/' + 'happy/' +\
								 os.path.splitext(file)[0] +'_h'+ os.path.splitext(file)[1]
				relaxed_filename = Config.working_dir+type + 'Features/' + 'relaxed/' + \
								   os.path.splitext(file)[0] +'_r'+ os.path.splitext(file)[1]
				neutral_filename = Config.working_dir+type + 'Features/' + 'neutral/' +\
								   os.path.splitext(file)[0] +'_n'+ os.path.splitext(file)[1]
				sad_filename = Config.working_dir+type + 'Features/' + 'sad/' + \
							   os.path.splitext(file)[0] +'_s'+ os.path.splitext(file)[1]
				angry_filename = Config.working_dir+type + 'Features/' + 'angry/' + \
								 os.path.splitext(file)[0] +'_a'+ os.path.splitext(file)[1]

				if (len(happy_frames) > 0):
					bob.io.save(happy_frames, happy_filename, create_directories=True)
				if (len(relaxed_frames) > 0):
					bob.io.save(relaxed_frames, relaxed_filename, create_directories=True)
				if (len(neutral_frames) > 0):
					bob.io.save(neutral_frames, neutral_filename, create_directories=True)
				if (len(sad_frames) > 0):
					bob.io.save(sad_frames, sad_filename, create_directories=True)
				if (len(angry_frames) > 0):
					bob.io.save(angry_frames, angry_filename, create_directories=True)

				features_output_file = Config.working_dir+'/'+type + 'Features/' + file
				logging.debug('processed channeled features file: '+features_output_file)
				print('processed channeled features file: '+features_output_file)
			else:
				# read file data
				filedata = audio_processing.iter_load_csv(channeledFeaturesPath+file)
				emotions = filedata[:,Config.num_features+2]
				emotions = np.array(emotions.flatten(),'int8')

				# split frames according to their emotions
				happy_frames = filedata[emotions == 1][:,:Config.num_features]
				relaxed_frames = filedata[emotions == 2][:,:Config.num_features]
				neutral_frames = filedata[emotions == 3][:,:Config.num_features]
				sad_frames = filedata[emotions == 4][:,:Config.num_features]
				angry_frames = filedata[emotions == 5][:,:Config.num_features]

				# create partial files from the current session
				happy_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/happy/' +\
								 os.path.splitext(file)[0] +'_h'+ os.path.splitext(file)[1]
				relaxed_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/relaxed/' + \
								   os.path.splitext(file)[0] +'_r'+ os.path.splitext(file)[1]
				neutral_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/neutral/' +\
								   os.path.splitext(file)[0] +'_n'+ os.path.splitext(file)[1]
				sad_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/sad/' + \
							   os.path.splitext(file)[0] +'_s'+ os.path.splitext(file)[1]
				angry_filename = Config.working_dir+type + 'Features/' +Config.test_speaker+ '/angry/' + \
								 os.path.splitext(file)[0] +'_a'+ os.path.splitext(file)[1]

				if (len(happy_frames) > 0):
					bob.io.save(happy_frames, happy_filename, create_directories=True)
				if (len(relaxed_frames) > 0):
					bob.io.save(relaxed_frames, relaxed_filename, create_directories=True)
				if (len(neutral_frames) > 0):
					bob.io.save(neutral_frames, neutral_filename, create_directories=True)
				if (len(sad_frames) > 0):
					bob.io.save(sad_frames, sad_filename, create_directories=True)
				if (len(angry_frames) > 0):
					bob.io.save(angry_frames, angry_filename, create_directories=True)

				features_output_file = Config.working_dir+'/'+type + 'Features/' + file
				logging.debug('processed channeled features file: '+features_output_file)
				print('processed channeled features file: '+features_output_file)

	logging.debug('Done splitting sessions into smaller files')
	print('Done splitting sessions into smaller files')

def convolveTestSpeaker():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting convoluting noised speech of test speaker with test IR...')
	print('Starting convoluting noised speech of test speaker with test IR...')
	# Convolute noised speech files with random impulse reaction
	for subdir, dirs, files in os.walk(Config.noised_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		# parallelize
		with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
			for w in wavs:
				if (w.find(Config.test_speaker) != -1):
					executor.submit(execConvolutionTestSpeaker, subdir, w)
	logging.info('Finished convoluting speech!')
	print('Finished convoluting speech!')

def execConvolutionTestSpeaker(subdir, w):
	speech_file = subdir + '/' + w
	# pick test impulse reaction file
	ir_file = Config.ir_int_dir+Config.test_ir

	# convolution
	result_file_path = speech_file.replace(Config.noised_dir, Config.convolute_dir)
	result_ir_filename = os.path.splitext(os.path.basename(result_file_path))[0] + \
					 '_' + os.path.splitext(os.path.basename(ir_file))[0] + '.wav'
	path,fname = os.path.split(result_file_path)
	result_file = path + '/' + result_ir_filename
	logging.debug('speech_file='+speech_file)
	print('speech_file='+speech_file)
	logging.debug('ir_file    ='+ir_file)
	print('ir_file    ='+ir_file)
	logging.debug('result_file='+result_file)
	print('result_file='+result_file)
	audio_processing.convolute_ir(speech_file, ir_file, result_file)


