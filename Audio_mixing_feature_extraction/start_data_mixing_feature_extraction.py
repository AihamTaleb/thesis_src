"""
This script represents the starting point for audio mixing
and feature extraction for the emotion and other datasets.
"""

import semaine_processing
import fau_processing
import timit_processing
import voxforge_processing
import configuration

# initializing test speakers and impulse responses (which to use)
Semaine_test_speakers = ['Obadiah','Spike','Prudence','Poppy']
FAU_test_speakers = ['Ohm_10','Mont_07','Mont_08','Ohm_18']
ir_names = ['none','rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche']

#### Semaine ####
print '**** Semaine ****'
dataset = 'Semaine'
for test_speaker in Semaine_test_speakers:
    for ir_name in ir_names:
        configuration.setConfiguration(ir_name, dataset, test_speaker)
        semaine_processing.processAudioDataset()
        semaine_processing.createCleanLearningDataset()
        if (ir_name != 'none'):
            semaine_processing.createChanneledLearningDataset()
        semaine_processing.splitSessionsToEmotionFiles()

#### FAU ####
print '**** FAU ****'
dataset = 'FAU'
for test_speaker in FAU_test_speakers:
    for ir_name in ir_names:
        configuration.setConfiguration(ir_name, dataset, test_speaker)
        fau_processing.processAudioDataset()
        fau_processing.createCleanLearningDataset()
        if (ir_name != 'none'):
            fau_processing.createChanneledLearningDataset()

#### Universal Background Model ####
print '**** ubm ****'
type = 'ssh'
dataset = 'ubm'
configuration.setConfiguration(ir_name, dataset)
timit_processing.processAudioDataset()
timit_processing.createCleanLearningDataset()
timit_processing.createChanneledLearningDataset()

#### Voice Activity Detection ####
print '**** vad ****'
type = 'ssh'
dataset = 'vad'
configuration.setConfiguration(ir_name, dataset)
timit_processing.processAudioDataset()
timit_processing.createCleanLearningDataset()
timit_processing.createChanneledLearningDataset()

#### Voxforge ####
print '**** voxforge ****'
type = 'ssh'
dataset = 'voxforge'
configuration.setConfiguration(ir_name, dataset)
voxforge_processing.processAudioDataset()
voxforge_processing.createCleanLearningDataset()
voxforge_processing.createChanneledLearningDataset()