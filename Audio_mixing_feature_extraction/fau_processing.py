"""
This script processes the emotion dataset FAU Aibo
"""
__author__ = 'aiham'


import logging
import audio_processing
import random
import os
import bob
from configuration import Config
from concurrent.futures import ThreadPoolExecutor
import multiprocessing

def processAudioDataset():

	# Configure logging
	logging_path = Config.working_dir + 'debug.log'
	logging.basicConfig(filename=logging_path, level=logging.DEBUG, format='%(asctime)s %(message)s')

	logging.info('Starting normalizing speech files...')
	print('Starting normalizing speech files...')
	for subdir, dirs, files in os.walk(Config.speech_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			speech_file = subdir+'/'+w
			result_file = speech_file.replace(Config.speech_dir, Config.normalize_dir)
			logging.debug('speech_file='+speech_file)
			print('speech_file='+speech_file)
			logging.debug('result_file='+result_file)
			print('result_file='+result_file)
			audio_processing.normalize_vs_noise(speech_file, Config.ref_noise, result_file)
	logging.info('Finished normalizing speech files!')
	print('Finished normalizing speech files!')

	logging.info('Starting transformation of 64bit float noise files to 16bit int...')
	print('Starting transformation of 64bit float noise files to 16bit int...')
	# Transform 64bit float noise files to 16bit int
	for subdir, dirs, files in os.walk(Config.noise_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			float_wav_file = subdir+w
			int_wav_file = float_wav_file.replace(Config.noise_dir, Config.noise_int_dir)
			logging.debug('float_wav_file ='+float_wav_file)
			print('float_wav_file ='+float_wav_file)
			logging.debug('int_wav_file='+int_wav_file)
			print('int_wav_file='+int_wav_file)
			audio_processing.float2int(float_wav_file, int_wav_file)
	logging.info('Finished transformation of data type!')
	print('Finished transformation of data type!')

	logging.info('Starting transformation of 64bit float ir files to 16bit int...')
	print('Starting transformation of 64bit float ir files to 16bit int...')
	# Transform 64bit float ir files to 16bit int
	for subdir, dirs, files in os.walk(Config.ir_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		for w in wavs:
			float_wav_file = subdir+w
			int_wav_file = float_wav_file.replace(Config.ir_dir, Config.ir_int_dir)
			logging.debug('float_wav_file ='+float_wav_file)
			print('float_wav_file ='+float_wav_file)
			logging.debug('int_wav_file='+int_wav_file)
			print('int_wav_file='+int_wav_file)
			audio_processing.float2int(float_wav_file, int_wav_file)
	logging.info('Finished transformation of data type!')
	print('Finished transformation of data type!')

	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting mixing speech files with random noise...')
	print('Starting mixing speech files with random noise...')
	# Mix speech files with random noise
	for subdir, dirs, files in os.walk(Config.normalize_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
			for w in wavs:
				speech_file = subdir+'/'+w
				result_file = speech_file.replace(Config.normalize_dir, Config.noised_dir)
				# pick random noise file, must be 16bit int, 64bit int doesn't work!
				noise_candidates = audio_processing.filtr(os.listdir(Config.noise_int_dir),'.wav')
				noise_file = Config.noise_int_dir+random.choice(noise_candidates)
				logging.debug('speech_file='+speech_file)
				print('speech_file='+speech_file)
				logging.debug('noise_file ='+noise_file)
				print('noise_file ='+noise_file)
				logging.debug('result_file='+result_file)
				print('result_file='+result_file)
				executor.submit(audio_processing.add_noise,speech_file, noise_file, result_file)
	logging.info('Finished mixing speech files!')
	print('Finished mixing speech files!')

	if (Config.ir_name != 'none'):
		logging.info('')
		print('')
		logging.info('')
		print('')
		logging.info('Starting convoluting noised speech with random IR...')
		print('Starting convoluting noised speech with random IR...')
		# Convolute noised speech files with random impulse reaction
		for subdir, dirs, files in os.walk(Config.noised_dir):
			# iterate over all wavs in the subdirectories
			wavs = audio_processing.filtr(files, 'wav')
			# parallelize
			with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
				for w in wavs:
					if (w.find(Config.test_speaker) == -1):
						executor.submit(execConvolution, subdir,w)
		logging.info('Finished convoluting speech!')
		print('Finished convoluting speech!')

		convolveTestSpeaker()

def execConvolution(subdir, w):
	speech_file = subdir + '/' + w
	# pick random impulse reaction file
	ir_file = Config.ir_int_dir+random.choice(audio_processing.filtr(os.listdir(Config.ir_int_dir),'.wav'))

	# making sure the chosen ir is different from testing ir
	while (ir_file.find(Config.test_pos)!=-1 or ir_file.find(Config.test_ir) != -1):
		ir_file = Config.ir_int_dir+random.choice(audio_processing.filtr(os.listdir(Config.ir_int_dir),'.wav'))

	# convolution
	result_file_path = speech_file.replace(Config.noised_dir, Config.convolute_dir)
	result_ir_filename = os.path.splitext(os.path.basename(result_file_path))[0] + \
					 '_' + os.path.splitext(os.path.basename(ir_file))[0] + '.wav'
	path,fname = os.path.split(result_file_path)
	result_file = path + '/' + result_ir_filename
	logging.debug('speech_file='+speech_file)
	print('speech_file='+speech_file)
	logging.debug('ir_file    ='+ir_file)
	print('ir_file    ='+ir_file)
	logging.debug('result_file='+result_file)
	print('result_file='+result_file)
	audio_processing.convolute_ir(speech_file, ir_file, result_file)

def execConvolutionTestSpeaker(subdir, w):
	speech_file = subdir + '/' + w
	# pick test impulse reaction file
	ir_file = Config.ir_int_dir+Config.test_ir

	# convolution
	result_file_path = speech_file.replace(Config.noised_dir, Config.convolute_dir+Config.test_speaker+'/')
	result_ir_filename = os.path.splitext(os.path.basename(result_file_path))[0] + \
					 '_' + os.path.splitext(os.path.basename(ir_file))[0] + '.wav'
	path,fname = os.path.split(result_file_path)
	result_file = path + '/' + result_ir_filename
	logging.debug('speech_file='+speech_file)
	print('speech_file='+speech_file)
	logging.debug('ir_file    ='+ir_file)
	print('ir_file    ='+ir_file)
	logging.debug('result_file='+result_file)
	print('result_file='+result_file)
	audio_processing.convolute_ir(speech_file, ir_file, result_file)

def createCleanLearningDataset():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	type='clean'
	for subdir, dirs, files in os.walk(Config.noised_dir):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			if (file.find(Config.test_speaker) == -1):
				try:
					# Calculate MFCCS for file
					frame_mfccs = audio_processing.process_speech(Config.noised_dir+file)

					name = file.replace('.wav','')
					features_output_file = Config.working_dir+'/'+type + 'Features/'+getFauEmotion(name)+'/'+name+'.csv'
					logging.debug('clean features files: '+features_output_file)
					print('clean features files: '+features_output_file)
					bob.io.save(frame_mfccs, features_output_file, create_directories=True)
				except:
					pass


	cleanFeaturesTestSpeaker()

	print('Saved clean speech MFCCS!')
	logging.info('Saved clean speech MFCCS!')

def createChanneledLearningDataset():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	type='channeled'
	for subdir, dirs, files in os.walk(Config.convolute_dir):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			if (file.find(Config.test_speaker) == -1):
				try:
					# Calculate MFCCS for file
					frame_mfccs = audio_processing.process_speech(Config.convolute_dir+file)

					name = file.replace('.wav','')
					# Mont_20_113_00
					# Ohm_02_208_00
					if (name[0] == 'M'):
						name = name[:14]
					elif (name[0] == 'O'):
						name = name[:13]
					features_output_file = Config.working_dir+'/'+type + 'Features/' + getFauEmotion(name) + \
										   '/' + file.replace('.wav', '') + '.csv'
					logging.debug('channeled features files: '+features_output_file)
					print('channeled features files: '+features_output_file)
					bob.io.save(frame_mfccs, features_output_file, create_directories=True)
				except:
					pass

	print('Saved speech MFCCS!')
	logging.info('Saved speech MFCCS!')

	featuresTestSpeaker()

def getFauEmotion(filename):
	if (Config.fau_labels_dict[filename] == 'N'):
		return 'Neutral'
	elif (Config.fau_labels_dict[filename] == 'P'):
		return 'Joyful'
	elif (Config.fau_labels_dict[filename] == 'E'):
		return 'Emphatic'
	elif (Config.fau_labels_dict[filename] == 'R'):
		return 'Other'
	elif (Config.fau_labels_dict[filename] == 'A'):
		return 'Angry'

def convolveTestSpeaker():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting convoluting noised speech of test speaker with test IR...')
	print('Starting convoluting noised speech of test speaker with test IR...')
	# Convolute noised speech files with random impulse reaction
	for subdir, dirs, files in os.walk(Config.noised_dir):
		# iterate over all wavs in the subdirectories
		wavs = audio_processing.filtr(files, 'wav')
		# parallelize
		with ThreadPoolExecutor(multiprocessing.cpu_count()) as executor:
			for w in wavs:
				if (w.find(Config.test_speaker) != -1):
					executor.submit(execConvolutionTestSpeaker, subdir, w)
	logging.info('Finished convoluting speech!')
	print('Finished convoluting speech!')

def cleanFeaturesTestSpeaker():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	testSpeakerPath = Config.noised_dir+'/'

	type='clean'
	for subdir, dirs, files in os.walk(testSpeakerPath):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			if (file.find(Config.test_speaker) != -1):
				try:
					# Calculate MFCCS for file
					frame_mfccs = audio_processing.process_speech(testSpeakerPath+file)

					name = file.replace('.wav','')
					# Mont_20_113_00
					# Ohm_02_208_00
					if (name[0] == 'M'):
						name = name[:14]
					elif (name[0] == 'O'):
						name = name[:13]
					features_output_file = Config.working_dir+'/'+type + 'Features/'+Config.test_speaker+'/'+ \
										   getFauEmotion(name) + '/' + file.replace('.wav', '') + '.csv'
					logging.debug('clean features files: '+features_output_file)
					print('clean features files: '+features_output_file)
					bob.io.save(frame_mfccs, features_output_file, create_directories=True)
				except:
					pass

	print('Saved speech MFCCS!')
	logging.info('Saved speech MFCCS!')

def featuresTestSpeaker():
	logging.info('')
	print('')
	logging.info('')
	print('')
	logging.info('Starting extraction of speech MFCCS & adaption of annotations...')
	print('Starting extraction of speech MFCCS & adaption of annotations...')

	testSpeakerPath = Config.convolute_dir+Config.test_speaker+'/'

	type='channeled'
	for subdir, dirs, files in os.walk(testSpeakerPath):
		# iterate over all wavs in the subdirectories
		soundfiles = audio_processing.filtr(files, 'wav')
		for file in soundfiles:
			try:
				# Calculate MFCCS for file
				frame_mfccs = audio_processing.process_speech(testSpeakerPath+file)

				name = file.replace('.wav','')
				# Mont_20_113_00
				# Ohm_02_208_00
				if (name[0] == 'M'):
					name = name[:14]
				elif (name[0] == 'O'):
					name = name[:13]
				features_output_file = Config.working_dir+'/'+type + 'Features/'+Config.test_speaker+'/'+ \
									   getFauEmotion(name) + '/' + file.replace('.wav', '') + '.csv'
				logging.debug('channeled features files: '+features_output_file)
				print('channeled features files: '+features_output_file)
				bob.io.save(frame_mfccs, features_output_file, create_directories=True)
			except:
				pass

	print('Saved speech MFCCS!')
	logging.info('Saved speech MFCCS!')
