__author__ = 'aiham'

import numpy as np
import time

def gmm_classifier(settings):
	(base_path_features, frame_inc, sampling_rate, k_fold, n_jobs_kmeans, \
		num_mixtures, num_features, gmm_covtype, num_iter_em, parameter_file_config_samples, \
		training_modes_path, test_speakers_path, ir_name,mode_corpus_name, frames_per_utterance, \
		balance_data, gmm_training_mode) = settings

	#################
	### read data ###
	#################
	from read_data import read_modes_unknown_speakers
	settings_data = (base_path_features, training_modes_path, test_speakers_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, \
		frames_per_utterance, balance_data)
	# ubm, mode training data, and test data from one speaker.
	(all_training_modes_samples, all_test_speakers_modes_samples, all_modes_training_partitions, \
		all_test_speaker_data_partitions) = read_modes_unknown_speakers(settings_data)

	no_of_modes = len(all_training_modes_samples)
	num_dimension = num_features

	#################################
	#### K-Fold cross validation ####
	#################################
	from auxiliary_functions import train_gmm
	all_predictions = []
	all_labels = []
	for cross_index in range(k_fold):
		print('processing fold number %d/%d' % (cross_index+1,k_fold))
		### Training personal models
		mode_personal_gmms = []
		print 'Personal training of mode GMMs...'
		ts_0 = time.time()
		for mode_index in range(no_of_modes):
			print 'training for mode (%d/%d)' % (mode_index+1,no_of_modes)
			# build training data mode
			mode_training_partitions = all_modes_training_partitions[mode_index]
			training_partition_indices = mode_training_partitions[cross_index]
			mode_samples = all_training_modes_samples[mode_index]
			mode_training_samples = np.float32(mode_samples[training_partition_indices])
			print('total training samples in this mode = %d' % len(mode_training_samples))

			gmm_settings = (num_mixtures, num_dimension, gmm_covtype, n_jobs_kmeans,
			   mode_training_samples, num_iter_em, ir_name, gmm_training_mode, frames_per_utterance)
			mode_gmm = train_gmm(gmm_settings)

			mode_personal_gmms.append(mode_gmm)
		ts_1 = time.time()
		print 'modes training done. Time passed: %fs' % (ts_1-ts_0)

		##################
		### Evaluation ###
		##################
		print 'Evaluation of modes gmms over this fold\'s test samples...'
		from auxiliary_functions import prepare_eval_data
		for test_speaker_index,test_speaker_samples in enumerate(all_test_speakers_modes_samples):
			# preparing test samples and labels
			ts_modes_samples_eval = test_speaker_samples
			(test_samples, true_labels) = \
				prepare_eval_data(ts_modes_samples_eval, \
					  all_test_speaker_data_partitions[test_speaker_index], no_of_modes, cross_index)

			# frames
			# predicted_labels = make_predictions_gmm_frames(mode_personal_gmms, test_samples, no_of_modes,\
			# 										num_features)
			# all_labels.extend(true_labels)
			# all_predictions.extend(predicted_labels)

			# utterances
			(fold_labels,fold_predictions) = make_predictions_gmm_utterances(mode_personal_gmms,
				 test_samples, true_labels, frames_per_utterance, no_of_modes, gmm_training_mode)
			all_labels.extend(fold_labels)
			all_predictions.extend(fold_predictions)

		print('done processing fold number %d' % cross_index)

	print 'Evaluation of this set is done. Calculating metrics...'
	from sklearn.metrics import recall_score, precision_score, f1_score, confusion_matrix, accuracy_score
	# recall
	mode_recalls = recall_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_recall = recall_score(all_labels, all_predictions, pos_label=None, average='macro')
	# precision
	mode_precisions = precision_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_precision = precision_score(all_labels, all_predictions, pos_label=None, average='macro')
	# f1
	mode_f1s = f1_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_f1 = f1_score(all_labels, all_predictions, pos_label=None, average='macro')
	# accuracy
	accuracy = accuracy_score(all_labels, all_predictions)
	# confusion matrix
	confusion_matrix = confusion_matrix(all_labels, all_predictions)

	print 'Unweighted average recall is : %f' % (unweighted_avg_recall)
	print 'Unweighted average precision is : %f' % (unweighted_avg_precision)
	print 'Unweighted average f1 is : %f' % (unweighted_avg_f1)
	print 'accuracy is : %f' % (accuracy)
	print 'mode-specific recalls are: '
	print (mode_recalls)
	print 'mode-specific precisions are: '
	print (mode_precisions)
	print 'mode-specific f1 scores are: '
	print (mode_f1s)
	print 'Confusion matrix for this run is: '
	print (confusion_matrix)

	print '*********************'
	return (mode_recalls, unweighted_avg_recall, confusion_matrix, accuracy, \
			mode_precisions, unweighted_avg_precision, mode_f1s, unweighted_avg_f1)

def make_predictions_gmm_frames(mode_personal_gmms, test_samples, no_of_modes, num_features):

	fold_predictions=[]
	ts_0 = time.time()
	print 'total number of test samples = %d' % len(test_samples)
	for test_index,test_sample in enumerate(test_samples):
		# Compute score of projected vector against personal models.
		# And predict mode according to maximum similarity
		max_score = -100000; predicted_label = 0
		for mode_index in range(no_of_modes):
			mode_gmm = mode_personal_gmms[mode_index]
			mean_log_lklds_fast_mode = np.mean(mode_gmm.score(test_sample.reshape(1,num_features)))
			score = mean_log_lklds_fast_mode
			if (score > max_score):
				max_score = score
				predicted_label = mode_index

		fold_predictions.append(int(predicted_label))
		if (test_index == 0):
			t_1 = time.time()
			print 'Evaluation of first test vector took: %fs' % (t_1-ts_0)

	ts_1 = time.time()
	print 'Evalutaion of this fold\'s test data done. Time passed: %fs' % (ts_1-ts_0)

	return fold_predictions

def make_predictions_gmm_utterances(mode_personal_gmms, test_samples, \
		test_labels, frames_per_utterance, no_of_modes, gmm_training_mode):

	from auxiliary_functions import norm_utt

	fold_predictions=[]; fold_labels = []
	ts_0 = time.time()
	num_utt = int(round(len(test_samples)/frames_per_utterance))
	print 'total number of test utterances = %d' % num_utt
	for utt_count in range(0, num_utt):
		data = test_samples[utt_count*frames_per_utterance \
			: (utt_count+1)*frames_per_utterance]

		data = norm_utt(data)

		# Compute score of projected vector against personal models.
		# And predict mode according to maximum similarity
		max_score = -100000; predicted_label = 0
		for mode_index in range(no_of_modes):
			mode_gmm = mode_personal_gmms[mode_index]
			mean_log_lklds_fast_mode = np.mean(mode_gmm.score(data))
			score = mean_log_lklds_fast_mode
			if (score > max_score):
				max_score = score
				predicted_label = mode_index

		# Calculate the true label for this utterance
		utt_labels = test_labels[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		# get mode (most common value) over all frames in this utterance
		true_label = round(np.mean(utt_labels))

		fold_labels.append(int(true_label))
		fold_predictions.append(int(predicted_label))

		if (utt_count == 0):
			t_1 = time.time()
			print 'Evaluation of first test vector took: %fs' % (t_1-ts_0)

	ts_1 = time.time()
	print 'Evalutaion of this fold\'s test data done. Time passed: %fs' % (ts_1-ts_0)

	return (fold_labels,fold_predictions)