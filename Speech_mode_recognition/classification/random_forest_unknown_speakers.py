__author__ = 'aiham'

import numpy as np
import time
from sklearn.ensemble import RandomForestClassifier

def random_forest_classifier(settings):
	(base_path_features, frame_inc, sampling_rate, k_fold, n_estimators, criterion,
		class_weight, min_samples_leaf, n_jobs, num_features, parameter_file_config_samples,
		training_modes_path, test_speakers_path, ir_name,mode_corpus_name, frames_per_utterance,
		balance_data) = settings

	#################
	### read data ###
	#################
	from read_data import read_modes_unknown_speakers
	settings_data = (base_path_features, training_modes_path, test_speakers_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, \
		frames_per_utterance, balance_data)
	(all_training_modes_samples, all_test_speakers_modes_samples, all_modes_training_partitions, \
		all_test_speaker_data_partitions) = read_modes_unknown_speakers(settings_data)

	no_of_modes = len(all_training_modes_samples)

	#################################
	#### K-Fold cross validation ####
	#################################
	from auxiliary_functions import prepare_training_data
	all_predictions = []
	all_labels = []
	for cross_index in range(k_fold):
		print('processing fold number %d/%d. Training data preparation...' % (cross_index+1,k_fold))
		ts_0 = time.time()

		(train_samples, train_labels) = \
			prepare_training_data(all_training_modes_samples, all_modes_training_partitions,
					  no_of_modes, cross_index,num_features,frames_per_utterance)
		print('Total training samples in this fold = %d' % len(train_samples))

		print("Fitting the random forest classifier on mode data...")
		forest = RandomForestClassifier(n_estimators = n_estimators, criterion = criterion, \
				min_samples_leaf = min_samples_leaf, n_jobs = n_jobs)#, class_weight = class_weight)
		forest.fit(train_samples, train_labels)
		print("Fitting done")

		ts_1 = time.time()
		print 'modes training done. Time passed: %fs' % (ts_1-ts_0)

		##################
		### Evaluation ###
		##################
		print 'Evaluation of modes gmms over this fold\'s test samples...'
		from auxiliary_functions import prepare_eval_data, normalize_features_window
		for test_speaker_index,test_speaker_samples in enumerate(all_test_speakers_modes_samples):
			(test_samples, true_labels) = \
				prepare_eval_data(test_speaker_samples,
					  all_test_speaker_data_partitions[test_speaker_index], no_of_modes, cross_index)

			# test_samples = normalize_features_window(test_samples,num_features,frames_per_utterance)
			# true_labels = true_labels[:len(test_samples)]

			# predicted_labels = forest.predict(test_samples)

			(fold_labels,fold_predictions) = make_predictions_forest(forest,test_samples,
									 true_labels, frames_per_utterance, no_of_modes)
			all_labels.extend(fold_labels)
			all_predictions.extend(fold_predictions)

			# all_labels.extend(true_labels)
			# all_predictions.extend(predicted_labels.astype(int))

		print 'done processing fold number %d' % (cross_index+1)

	print 'Evaluation of this set is done. Calculating metrics...'
	from sklearn.metrics import recall_score, precision_score, f1_score, confusion_matrix, accuracy_score
	# recall
	mode_recalls = recall_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_recall = recall_score(all_labels, all_predictions, pos_label=None, average='macro')
	# precision
	mode_precisions = precision_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_precision = precision_score(all_labels, all_predictions, pos_label=None, average='macro')
	# f1
	mode_f1s = f1_score(all_labels, all_predictions, pos_label=None, average=None)
	unweighted_avg_f1 = f1_score(all_labels, all_predictions, pos_label=None, average='macro')
	# accuracy
	accuracy = accuracy_score(all_labels, all_predictions)
	# confusion matrix
	confusion_matrix = confusion_matrix(all_labels, all_predictions)

	print 'Unweighted average recall is : %f' % (unweighted_avg_recall)
	print 'Unweighted average precision is : %f' % (unweighted_avg_precision)
	print 'Unweighted average f1 is : %f' % (unweighted_avg_f1)
	print 'accuracy is : %f' % (accuracy)
	print 'mode-specific recalls are: '
	print (mode_recalls)
	print 'mode-specific precisions are: '
	print (mode_precisions)
	print 'mode-specific f1 scores are: '
	print (mode_f1s)
	print 'Confusion matrix for this run is: '
	print (confusion_matrix)

	print '*********************'
	return (mode_recalls, unweighted_avg_recall, confusion_matrix, accuracy, \
			mode_precisions, unweighted_avg_precision, mode_f1s, unweighted_avg_f1)

def make_predictions_forest(forest, test_samples, test_labels, frames_per_utterance, no_of_modes):

	from auxiliary_functions import norm_utt

	fold_predictions=[]; fold_labels = []
	ts_0 = time.time()
	num_utt = int(round(len(test_samples)/frames_per_utterance))
	print 'total number of test utterances = %d' % num_utt
	for utt_count in range(0, num_utt):
		data = test_samples[utt_count*frames_per_utterance \
			: (utt_count+1)*frames_per_utterance]

		data = norm_utt(data)

		pred_utt = forest.predict(data)
		predicted_label = round(np.mean(pred_utt))

		utt_labels = test_labels[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		true_label = round(np.mean(utt_labels))

		fold_labels.append(int(true_label))
		fold_predictions.append(int(predicted_label))

		if (utt_count == 0):
			t_1 = time.time()
			print 'Evaluation of first test vector took: %fs' % (t_1-ts_0)

	ts_1 = time.time()
	print 'Evalutaion of this fold\'s test data done. Time passed: %fs' % (ts_1-ts_0)

	return (fold_labels,fold_predictions)