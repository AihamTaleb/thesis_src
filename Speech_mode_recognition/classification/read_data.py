__author__ = 'aihamtaleb'

import numpy as np
import os
from random import choice
import operator
from sklearn.neighbors import NearestNeighbors
from sklearn import cross_validation

# csv load
def iter_load_csv(filename, delimiter=',', max_samples=1.5E6, dtype=float):
	def iter_func():
		with open(filename, 'r') as infile:
			i = 0
			for line in infile:
				if line.strip():
					line = line.rstrip().split(delimiter)
					if i == max_samples:
						break
					i = i + 1
					for item in line:
						yield dtype(item)
					iter_load_csv.rowlength = len(line)

	data = np.fromiter(iter_func(), dtype=dtype)
	data = data.reshape((-1, iter_load_csv.rowlength))
	return data

def filtr(files, filetype):
	"""Filters a file list by the given filename ending to be accepted"""
	return filter(lambda d: 1 if d.endswith(filetype) else 0, files)

# read ubm and mode specific datasets
def read_modes_unknown_speakers(settings):
	(base_path_features, training_modes_path, test_speakers_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, \
		frames_per_utterance, balance_data) = settings

	#region Reading training data
	## mode models:
	# length of data to be read
	# max_length_in_min = 10000
	# max_length_in_samples = np.int32(np.ceil((max_length_in_min * 60 * sampling_rate) \
	# 								/ (frame_inc * sampling_rate / 1000 )))

	# load modes data
	all_modes_folders = os.walk(base_path_features + training_modes_path).next()[1]
	all_modes_folders.sort()

	# actual data
	all_training_modes_samples = []
	all_modes_training_partitions = []

	print 'Reading mode data for training...'
	#iterate over all modes
	for mode_index,mode in enumerate(all_modes_folders):
		# path of samples for mode
		mode_feature_path = base_path_features + training_modes_path + mode + '/' \
							   + parameter_file_config_samples + '.csv'

		# loading mode vad samples
		mode_vad_features = iter_load_csv(mode_feature_path)
		mode_samples = np.array(mode_vad_features[:,:num_features],'float32')
		mode_samples = np.nan_to_num(mode_samples)

		# set the maximum length of each mode
		# current_max_length = min(max_length_in_samples, len(mode_samples))
		# mode_samples = mode_samples[:current_max_length,:]

		print 'Reading mode %d (%s) done.' % (len(all_training_modes_samples), mode)

		# put training and test data of all modes into a list
		all_training_modes_samples.append(np.array(mode_samples))
	print 'Done reading mode data for training.'

	# constructing training partitions indices
	for mode_samples in all_training_modes_samples:
		cross_indices = []
		# split speaker data into folds for cv; from all voiced samples take last column (label)
		cross_indices[0:k_fold-1] = cross_validation.KFold(len(mode_samples), \
			n_folds=k_fold, shuffle=True)
		cross_indices = np.array(cross_indices)
		# create one array containing the training indices of all folds
		cross_training = cross_indices[:,0]
		# put training indices of all modes into a list
		all_modes_training_partitions.append(np.array(cross_training))
	#endregion

	#region Reading testing data
	print 'Reading test speakers mode samples...'
	## mode models:
	# length of data to be read
	max_length_in_min = 10
	max_length_in_samples = np.int32(np.ceil((max_length_in_min * 60 * sampling_rate) \
									/ (frame_inc * sampling_rate / 1000 )))

	all_test_speakers_folders = os.walk(base_path_features + test_speakers_path).next()[1]
	all_test_speakers_folders.sort()

	all_test_speakers_modes_samples = []
	for test_speaker in all_test_speakers_folders:
		test_speaker_samples = []
		all_modes_folders = os.walk(base_path_features+test_speakers_path+test_speaker).next()[1]
		all_modes_folders.sort()
		print 'Reading test speaker: %s modes samples' % test_speaker
		for mode_index,ts_mode in enumerate(all_modes_folders):
			# path of samples for mode
			ts_mode_feature_path = base_path_features + test_speakers_path + '/' + \
			   test_speaker + '/'+ ts_mode + '/' + parameter_file_config_samples + '.csv'

			# loading mode samples
			ts_mode_features = iter_load_csv(ts_mode_feature_path)
			ts_mode_samples = ts_mode_features[:,:num_features]
			ts_mode_samples = np.nan_to_num(np.array(ts_mode_samples, 'float32'))

			# set the maximum length of each mode
			current_max_length = min(max_length_in_samples, len(ts_mode_samples))
			ts_mode_samples = ts_mode_samples[:current_max_length,:]

			print 'Test speaker\'s mode  %d (%s) done' % (len(all_test_speakers_modes_samples), ts_mode)

			test_speaker_samples.append(np.array(ts_mode_samples))
		all_test_speakers_modes_samples.append(np.array(test_speaker_samples))
	print 'Done reading test speaker mode samples.'

	all_test_speaker_data_partitions = []
	# constructing testing partitions
	for test_speaker_samples in all_test_speakers_modes_samples:
		test_speaker_data_partitions = []
		for ts_mode_samples in test_speaker_samples:
			cross_indices = []
			# split speaker data into folds for cv; from all voiced samples take last column (label)
			cross_indices[0:k_fold-1] = cross_validation.KFold(len(ts_mode_samples), \
				n_folds=k_fold, shuffle=True)
			cross_indices = np.array(cross_indices)
			# create one array containing the training indices of all folds
			cross_testing = cross_indices[:,0]
			# put training indices of all modes into a list
			test_speaker_data_partitions.append(np.array(cross_testing))
		all_test_speaker_data_partitions.append(test_speaker_data_partitions)
	#endregion

	return (all_training_modes_samples, all_test_speakers_modes_samples, \
			all_modes_training_partitions, all_test_speaker_data_partitions)


# read ubm and mode specific datasets
def read_modes_known_speakers(settings):
	(base_path_features, training_modes_path, test_speakers_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, \
		frames_per_utterance, balance_data) = settings

	# load modes data
	all_modes_folders = os.walk(base_path_features + training_modes_path).next()[1]
	all_modes_folders.sort()

	# actual data
	all_training_modes_samples = []
	all_modes_training_partitions = []
	all_modes_test_partitions = []

	print 'Reading mode data for training...'
	#iterate over all modes
	for mode_index,mode in enumerate(all_modes_folders):
		# path of samples for mode
		mode_feature_path = base_path_features + training_modes_path + mode + '/' \
							   + parameter_file_config_samples + '.csv'

		# loading mode vad samples
		mode_vad_features = iter_load_csv(mode_feature_path)
		mode_samples = np.array(mode_vad_features[:,:num_features],'float32')
		mode_samples = np.nan_to_num(mode_samples)

		print 'Reading mode %d (%s) done.' % (len(all_training_modes_samples), mode)

		# put training and test data of all modes into a list
		all_training_modes_samples.append(np.array(mode_samples))
	print 'Done reading mode data for training.'

	# constructing partitions indices
	for mode_samples in all_training_modes_samples:
		cross_indices = []

		cross_indices[0:k_fold-1] = cross_validation.KFold(len(mode_samples), \
			n_folds=k_fold, shuffle=True)
		cross_indices = np.array(cross_indices)

		cross_training = cross_indices[:,0]
		cross_testing = cross_indices[:,1]

		all_modes_training_partitions.append(np.array(cross_training))
		all_modes_test_partitions.append(np.array(cross_testing))
	#endregion

	return (all_training_modes_samples, all_modes_training_partitions, all_modes_test_partitions)


######
## balance data
######
#region balance datasets
def balance_dataset(all_samples_voiced, all_targets, all_samples_voiced_flattened):
	counts = np.bincount(np.array(all_targets, 'int'))
	print('Classes distributions before balancing')
	print(counts)
	majority_index, no_majority_samples = max(enumerate(counts), key=operator.itemgetter(1))
	for mode_index in range(len(all_samples_voiced)):
		if mode_index == majority_index: continue
		if (counts[mode_index] > 0.9*float(no_majority_samples)):
			print('skipping mode %d as it has enough samples.' % mode_index)
			continue

		print("processing mode: %d" % mode_index)
		synthetic_size = np.random.randint( \
			(int(round(0.9*float(no_majority_samples))) - counts[mode_index]), \
			(no_majority_samples-counts[mode_index]))
		k = 20
		(synthetic_samples, danger_minorities) = \
			borderlineSMOTE(np.array(all_samples_voiced_flattened,'float32'), \
							all_targets, mode_index, synthetic_size, k)
		print("done balancing mode: %d" % mode_index)

		random_synthetic_indices = np.random.randint(len(synthetic_samples), size=synthetic_size)
		all_samples_voiced[mode_index] = \
			np.append(all_samples_voiced[mode_index], \
				  synthetic_samples[random_synthetic_indices], axis=0)

		counts[mode_index] = len(all_samples_voiced[mode_index])
	print('Classes distributions after balancing')
	print(counts)

	return all_samples_voiced

def borderlineSMOTE(X, y, minority_target, synthetic_size, k):
	"""
	Returns synthetic minority samples.
	Parameters
	----------
	X : array-like, shape = [n__samples, n_features]: Holds the minority and majority samples
	y : array-like, shape = [n__samples]: Holds the class targets for samples
	minority_target : value for minority class
	synthetic_size : number of new synthetic samples
	k : int. Number of nearest neighbours.
	-------
	synthetic : Synthetic sample of minorities in danger zone
	danger : Minorities of danger zone
	"""
	#Learn nearest neighbours on complete training set
	y = np.array(y,'int8').flatten()

	print("applying SMOTE algorithm on minority samples...")

	#SMOTE danger minority samples
	synthetic_samples = SMOTE(X[y == minority_target], synthetic_size, k, h = 0.5)

	return (synthetic_samples, X[y == minority_target])

def SMOTE(T, synthetic_size, k, h = 1.0):
	"""
	Returns "synthetic_size" synthetic minority samples.
	Parameters
	----------
	T : array-like, shape = [n_minority_samples, n_features]: Holds the minority samples.
	synthetic_size : amount of new synthetic samples: n_synthetic_samples = synthetic_size
	k : int. Number of nearest neighbours.
	Returns
	-------
	S : Synthetic samples. array,
		shape = [synthetic_size, n_features].
	"""
	n_minority_samples, n_features = T.shape
	n_synthetic_samples = synthetic_size
	S = np.zeros(shape=(n_synthetic_samples, n_features))

	#Learn nearest neighbours
	neigh = NearestNeighbors(n_neighbors = k)
	neigh.fit(T)

	#Calculate synthetic samples
	for i in xrange(n_synthetic_samples):
		minority_index = np.random.randint(n_minority_samples)
		nn = neigh.kneighbors(T[minority_index], return_distance=False)
		nn_index = choice(nn[0])
		#NOTE: nn includes T[i], we don't want to select it
		while nn_index == minority_index:
			nn_index = choice(nn[0])
		dif = T[nn_index] - T[minority_index]
		gap = np.random.uniform(low = 0.0, high = h)
		S[i, :] = T[minority_index,:] + gap * dif[:]

	return S
#endregion
