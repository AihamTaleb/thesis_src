__author__ = 'aiham'

mode_corpus_name = 'wTIMIT'

# testing_ir_names = ['rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche','none']
testing_ir_names = ['rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche']

## base features path
base_path_features = '/home/taleb/data/corpora_mfcc_dd/'

######################
### Configurations ###
######################
frame_length = 20
frame_inc = 10
num_features = 36
k_fold = 10
n_jobs_kmeans = 25
num_mixtures = 64
frames_per_utterance = 256
gmm_covtype = 'diag'
num_iter_em = 100
sampling_rate = 16000
balance_data = False

# either 'cpu' or 'gpu'
gmm_training_mode = 'gpu'

## filenames
#samples configs
samples_numb_features = 18
parameter_file_config_samples = 'm' + str(samples_numb_features) + 'w' \
	+ str(frame_length) + 'i' + str(frame_inc)

#############################
### Training & Evaluation ###
#############################
from gmm_unknown_speakers import gmm_classifier
metrics_list = []
clean_metrics_list = []

for ir_name in testing_ir_names:
    print '#### training and testing for IR = %s ###' % ir_name
    training_modes_path = ir_name+'/'+mode_corpus_name+'/eval/'
    test_speakers_path = ir_name+'/'+mode_corpus_name+'/test_speakers/'

    settings = (base_path_features, frame_inc, sampling_rate, k_fold, n_jobs_kmeans, \
        num_mixtures, num_features, gmm_covtype, num_iter_em, parameter_file_config_samples, \
        training_modes_path, test_speakers_path, ir_name,mode_corpus_name, frames_per_utterance, \
        balance_data, gmm_training_mode)
    result = gmm_classifier(settings)

    # if ir_name == 'none':
    #     clean_metrics_list.append(result)
    # else:
    metrics_list.append(result)

    print '#### done training and testing for IR = %s ###' % ir_name


from auxiliary_functions import calc_overall_performance

# (clean_unweighted_avg_recall, clean_avg_recalls_per_modes, clean_overall_confusion_matrix, \
# 	clean_overall_unweighted_avg_precision, clean_avg_precisions_per_modes, \
# 	clean_overall_unweighted_avg_f1, clean_avg_f1s_per_modes, clean_overall_acc) = \
# 	calc_overall_performance(clean_metrics_list)
#
# print '****************************************************************************************'
# print 'Evaluation on %s corpus (No-IRs) is done' % (mode_corpus_name)
# print 'Final unweighted average recall over all modes (clean) is %.2f' % (clean_unweighted_avg_recall)
# print 'Final unweighted average recall for all modes (clean) is %s' % \
# 	  (', '.join(["%0.2f" % i for i in clean_avg_recalls_per_modes]))
# print 'Final unweighted average precision over all modes (clean) %.2f' % (clean_overall_unweighted_avg_precision)
# print 'Final unweighted average precision for all modes (clean) is %s' % \
# 	  (', '.join(["%0.2f" % i for i in clean_avg_precisions_per_modes]))
# print 'Final unweighted average f1 over all modes (clean) is %.2f' % (clean_overall_unweighted_avg_f1)
# print 'Final unweighted average f1 for all modes (clean) is %s' % \
# 	  (', '.join(["%0.2f" % i for i in clean_avg_f1s_per_modes]))
# print 'Final accuracy over all modes (clean) is %.2f' % (clean_overall_acc)
# print 'Final overall confusion matrix over modes (clean) is : '
# print (clean_overall_confusion_matrix)
# print '****************************************************************************************'


(overall_unweighted_avg_recall, avg_recalls_per_modes, overall_confusion_matrix, \
	overall_unweighted_avg_precision, avg_precisions_per_modes, overall_unweighted_avg_f1, \
	avg_f1s_per_modes, overall_acc) = calc_overall_performance(metrics_list)

print '****************************************************************************************'
print 'Evaluation on %s corpus (with-IRs) is done' % (mode_corpus_name)
print 'Final unweighted average recall over all modes and sets is %.2f' % (overall_unweighted_avg_recall)
print 'Final unweighted average recall for all modes over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_recalls_per_modes]))
print 'Final unweighted average precision over all modes and sets is %.2f' % (overall_unweighted_avg_precision)
print 'Final unweighted average precision for all modes over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_precisions_per_modes]))
print 'Final unweighted average f1 over all modes and sets is %.2f' % (overall_unweighted_avg_f1)
print 'Final unweighted average f1 for all modes over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_f1s_per_modes]))
print 'Final accuracy over all modes and sets is %.2f' % (overall_acc)
print 'Final overall confusion matrix over all sets and modes is : '
print (overall_confusion_matrix)
print '****************************************************************************************'