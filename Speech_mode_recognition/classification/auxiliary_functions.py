__author__ = 'aihamtaleb'

import numpy as np
import random
import time
import os
import datetime

from gmm_specializer.gmm import GMM
from sklearn.cluster import KMeans
from sklearn.mixture import GMM as GMM_CPU

def train_gmm(gmm_settings):
	(num_mixtures, num_dimension, gmm_covtype, n_jobs_kmeans, mode_training_samples,
     num_iter_em, ir_name, gmm_training_mode, frames_per_utterance) = gmm_settings

	mode_training_samples = normalize_features_window(mode_training_samples,num_dimension,
                                      frames_per_utterance)

	if (gmm_training_mode == 'gpu'):
		gmm = train_gmm_cuda(num_mixtures, num_dimension, gmm_covtype, n_jobs_kmeans,
						  mode_training_samples, num_iter_em)
	else:
		gmm = train_gmm_cpu(num_mixtures, gmm_covtype, mode_training_samples, num_iter_em, n_jobs_kmeans)

	return gmm


def train_gmm_cuda(num_mixtures, num_dimension, gmm_covtype, n_jobs_kmeans, \
						  mode_training_samples, num_iter_em):

	init_mean_ubm, init_cov_ubm = init_with_kmeans(num_mixtures, num_dimension, \
		n_jobs_kmeans, mode_training_samples)
	init_weight_ubm = np.ones(num_mixtures)
	init_weight_ubm = init_weight_ubm * (1/np.float32(num_mixtures))
	init_weight_ubm = np.array(init_weight_ubm,dtype=np.float32)

	print 'Starting GMM training on the GPU...'
	tic = time.time()
	gmm_fast_ubm = GMM(num_mixtures, num_dimension, cvtype=gmm_covtype, means=init_mean_ubm, \
		covars=init_cov_ubm, weights=init_weight_ubm)
	gmm_fast_ubm.train(mode_training_samples,max_em_iters=num_iter_em, min_em_iters=num_iter_em)
	toc = time.time()
	print 'GMM training done. Time passed: %fs' % (toc-tic)

	return gmm_fast_ubm


def train_gmm_cpu(num_mixtures, gmm_covtype, mode_training_samples, num_iter_em, n_jobs_kmeans):
	print 'Starting GMM training on the CPU...'

	tic = time.time()
	print 'Init K-Means for GMM means ...'
	initKMeans = KMeans(n_clusters=num_mixtures, init='k-means++', n_init=1, max_iter=5, \
		verbose=0, random_state=1, n_jobs=n_jobs_kmeans)
	initKMeans.fit(mode_training_samples)
	print 'Finished init K-Means for GMM means.'

	print 'GMM Training...'
	gmm = GMM_CPU(n_components=num_mixtures, covariance_type=gmm_covtype, \
					   n_iter=num_iter_em, init_params='wc')
	#cluster centers are mean values
	gmm.means_ = initKMeans.cluster_centers_
	gmm.fit(mode_training_samples)
	toc = time.time()
	print 'GMM training done. Time passed: %fs' % (toc-tic)

	return gmm


#kmeans to initialize the parameters of the gmm
def init_with_kmeans(num_mixtures, num_dimension, n_jobs_kmeans, samples):
	print 'k-means init on the CPU...'
	initKMeans = KMeans(n_clusters=num_mixtures, init='k-means++', n_init=1, max_iter=5, \
		verbose=0, random_state=1, n_jobs=n_jobs_kmeans)
	initKMeans.fit(samples)
	#cluster center are mean values
	init_mean = initKMeans.cluster_centers_

	#calc covariance matrix for each cluster/mixture
	init_cov = np.zeros((num_mixtures,num_dimension,num_dimension))
	for i in range(num_mixtures):
		cluster_index = i
		samples_of_cluster = samples[initKMeans.labels_ == cluster_index]
		if len(samples_of_cluster) > 0:
			cov_of_cluster = np.cov(samples_of_cluster.T)
			diag_cov = cov_of_cluster * np.identity(num_dimension)
		else:
			diag_cov = np.identity(num_dimension)
		init_cov[i,:,:] = diag_cov

	init_mean = np.array(init_mean,dtype=np.float32)
	init_cov =  np.array(init_cov,dtype=np.float32)

	print 'Done k-means init on the CPU.'

	return init_mean, init_cov


def prepare_training_data(modes_training_samples, all_training_partitions, \
		   no_of_modes, cross_index,num_features,frames_per_utterance):
	#build train data (one single training vector)
	train_chunks_all_modes = []

	for mode_index in range(no_of_modes):
		#get train data of mode
		mode_train_partitions = all_training_partitions[mode_index]
		train_partitions_indices = mode_train_partitions[cross_index]
		mode_samples = modes_training_samples[mode_index]
		train_samples = np.float32(mode_samples[train_partitions_indices]).tolist()

		#cut train data into more realistic utterance-like chunks
		while train_samples:
			#randomly pick size of next chunk from range 400 to 2000 frames.
			#for frame_length=20 and increment=10 this amounts to roughly 4-20 secs.
			chunk_size = random.randint(400, 2000)
			#don't get bigger chunk than number of elements in the list
			if chunk_size >= len(train_samples):
				train_chunks_all_modes.append((train_samples,np.full(len(train_samples), mode_index)))
				train_samples = []
			else:
				chunk = np.array(train_samples[:chunk_size])
				del train_samples[:chunk_size]
				train_chunks_all_modes.append((chunk,np.full(chunk_size, mode_index)))

	# random.shuffle(train_chunks_all_modes)

	#build lists from tuples and flatten, e.g. [[0,0,0],[1,1,1]] -> [0,0,0,1,1,1]
	train_samples = [item for (samp,lab) in train_chunks_all_modes for item in samp]
	train_labels = [item for (samp,lab) in train_chunks_all_modes for item in lab]

	train_samples = normalize_features_window(train_samples,num_features,frames_per_utterance)
	train_labels = train_labels[:len(train_samples)]

	return (np.float32(train_samples), np.int8(train_labels))


def prepare_eval_data(ts_modes_samples_voiced_eval, all_test_data_partitions, \
							   no_of_modes, cross_index):
	#build test data (one single test vector)
	test_chunks_all_modes = []

	for mode_index_for_test in range(no_of_modes):
		#get test data of mode
		ts_mode_test_partitions = all_test_data_partitions[mode_index_for_test]
		test_partition_index = ts_mode_test_partitions[cross_index]
		mode_samples = ts_modes_samples_voiced_eval[mode_index_for_test]
		test_samples = np.float32(mode_samples[test_partition_index]).tolist()

		#cut test data into more realistic utterance-like chunks
		while test_samples:
			#randomly pick size of next chunk from range 400 to 2000 frames.
			#for frame_length=20 and increment=10 this amounts to roughly 4-20 secs.
			chunk_size = random.randint(400, 2000)
			#don't get bigger chunk than number of elements in the list
			if chunk_size >= len(test_samples):
				test_chunks_all_modes.append((test_samples,np.full(len(test_samples), mode_index_for_test)))
				test_samples = []
			else:
				chunk = np.array(test_samples[:chunk_size])
				del test_samples[:chunk_size]
				test_chunks_all_modes.append((chunk,np.full(chunk_size, mode_index_for_test)))

	random.shuffle(test_chunks_all_modes)

	#build lists from tuples and flatten, e.g. [[0,0,0],[1,1,1]] -> [0,0,0,1,1,1]
	test_samples = [item for (samp,lab) in test_chunks_all_modes for item in samp]
	test_labels = [item for (samp,lab) in test_chunks_all_modes for item in lab]

	return (np.float32(test_samples), np.int8(test_labels))


#region normalize datasets

def l2_norm(voiced_data):
	try:
		tmp_std = np.std(voiced_data, 0)
		voiced_data -= np.mean(voiced_data, 0)
		voiced_data /= tmp_std
		return voiced_data
	except:
		voiced_data -= np.mean(voiced_data, 0)
		return voiced_data

def norm_utt(utt_data):
	try:
		tmp_std = np.std(utt_data, 0)
		utt_data -= np.mean(utt_data, 0)
		utt_data /= tmp_std
		return utt_data
	except:
		utt_data -= np.mean(utt_data, 0)
		return utt_data

def norm(a_i, b_i, c_i):
	try:
		return (a_i-b_i)/c_i
	except:
		return (a_i-b_i)

def normalize_features_window(data, num_features, \
						  frames_per_utterance, frame_norm_inc = 128):
	np.seterr(all='raise')
	# data = l2_norm(data)
	if (len(data) > frames_per_utterance):
		# split data into utterances and normalize each
		num_utt = int(round(len(data)/frames_per_utterance))
		for utt_count in range(0, num_utt):
			#get values from array, normalize, then update array
			utt_data = data[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance]
			utt_data = norm_utt(utt_data)
			data[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance] = utt_data

		#ignore remaining frames that don't fill the last window
		data = data[:num_utt*frames_per_utterance]
	else:
		# normalize features locally for each frame: by subtracting the mean of its
		# surrounding neighbors in a radius of "frame_norm_inc". And dividing by their std.
		original_data = np.copy(data)
		begin_index = 0
		frames_count = len(data)
		for i in range(frames_count):
			begin_index = min(frame_norm_inc, i-begin_index)
			end_index = min(frame_norm_inc, len(original_data)-i)
			neighbors_features_means = np.nanmean(original_data[i-begin_index:i+end_index, :num_features], axis=0)
			neighbors_features_stds = np.nanstd(original_data[i-begin_index:i+end_index, :num_features], axis=0)

			data[i,:num_features] = [norm(a_i,b_i,c_i) for a_i, b_i, c_i in \
									 zip(original_data[i,:num_features], neighbors_features_means, neighbors_features_stds)]

	return data


def create_results_folder(base_results_path):
	# if the folder already exists, append counter suffix
	date_str = str(datetime.date.today())
	results_path = base_results_path + 'SPEECH_MODE/GMM/' + date_str
	if  os.path.exists(results_path):
		counter = 2
		while os.path.exists(results_path + '_' + str(counter)):
			counter = counter + 1
		results_path = results_path + '_' + str(counter) + '/'
	else:
		results_path = results_path + '/'
	os.makedirs(results_path)
	print 'The designated result path for this run is %s.' % (results_path)
	return results_path


def calc_overall_performance(metrics_list):

	# recall
	mode_recalls_list = [a for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_recalls_per_modes = np.nanmean(mode_recalls_list, axis=0)
	overall_unweighted_avg_recall = np.nanmean([b for (a,b,c,d,e,f,g,h) in metrics_list])

	# precision
	mode_precisions_list = [e for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_precisions_per_modes = np.nanmean(mode_precisions_list, axis=0)
	overall_unweighted_avg_precision = np.nanmean([f for (a,b,c,d,e,f,g,h) in metrics_list])

	# f1
	mode_f1s_list = [g for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_f1s_per_modes = np.nanmean(mode_f1s_list, axis=0)
	overall_unweighted_avg_f1 = np.nanmean([h for (a,b,c,d,e,f,g,h) in metrics_list])

	# accuracy
	overall_acc = np.nanmean([d for (a,b,c,d,e,f,g,h) in metrics_list])

	# confusion matrix
	overall_confusion_matrix = np.sum([c for (a,b,c,d,e,f,g,h) in metrics_list], axis=0)

	return (overall_unweighted_avg_recall, avg_recalls_per_modes, overall_confusion_matrix, \
			overall_unweighted_avg_precision, avg_precisions_per_modes, overall_unweighted_avg_f1, \
			avg_f1s_per_modes, overall_acc)