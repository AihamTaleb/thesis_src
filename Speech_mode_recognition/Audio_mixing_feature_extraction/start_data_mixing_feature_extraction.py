import wTIMIT_processing
import configuration

lg_test_speakers = ['SG000','SG001','SG002','SG003']
s3_test_speakers = ['SG004','SG005','SG006','SG007']
s1_test_speakers = ['SG008','SG009','SG010','SG011']
htc_test_speakers = ['SG012','SG013','SG014','SG015']
none_test_speakers = ['SG016','SG017','SG018','SG019']

all_test_speakers = []
all_test_speakers.append(lg_test_speakers)
all_test_speakers.append(s3_test_speakers)
all_test_speakers.append(s1_test_speakers)
all_test_speakers.append(htc_test_speakers)
all_test_speakers.append(none_test_speakers)

ir_names = ['rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche','none']

print '**** wTIMIT ****'
dataset = 'wTIMIT'

#### wTIMIT ####
for ir_index, ir_name in enumerate(ir_names):
	print '********'
	print(ir_name)
	print '********'
	test_speakers = all_test_speakers[ir_index]
	configuration.setConfiguration(ir_name, dataset, test_speakers)
	# wTIMIT_processing.processAudioDataset()
	# wTIMIT_processing.createCleanLearningDataset()
	#if (not (ir_name == 'none')):
	wTIMIT_processing.createChanneledLearningDataset()