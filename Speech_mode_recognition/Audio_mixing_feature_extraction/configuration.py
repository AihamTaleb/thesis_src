import os
import audio_processing


class Config:
	type='local'
	code_dir = os.path.abspath(os.getcwd()) + '/'
	data_dir = code_dir.replace('SourceCode','Datasets/Berlin-EMO')
	speech_dir = data_dir + 'wav/'

	support_dir = code_dir.replace('SourceCode','Datasets/Support')
	noise_dir = support_dir + 'train/'
	noise_int_dir = support_dir + 'noise_int/'
	ir_int_dir = support_dir + 'ir_int/'
	ir_dir = support_dir + 'impulse_responses/16kHz/wavs/'
	ref_noise = support_dir + 'ref_pink.wav'
	label_dir=speech_dir.replace('wav','labels/CEICES')+'/'+'chunk_labels_4cl_aibo_chunk_set.txt'

	working_dir = data_dir
	normalize_dir = working_dir + 'normalized/'
	noised_dir = working_dir + 'cleanData/'
	convolute_dir = working_dir + 'channeledData/'
	channeledData= 'channeledData'
	cleanData= 'cleanData'
	fau_labels_dict = {}
	num_features = 36
	test_speakers = []
	test_ir = ''
	test_pos = ''
	ir_name = ''

	# cepstral features config
	win_length_ms = 20
	win_shift_ms = 10
	n_filters = 24
	n_ceps = 12
	f_min = 0.
	f_max = 4000.
	delta_win = 5
	pre_emphasis_coef = 0.97
	dct_norm = True
	mel_scale = True
	with_energy = False
	with_delta = True
	with_delta_delta = True


def setConfiguration(ir_name,set,test_speakers):
	Config.code_dir = os.path.abspath(os.getcwd()) + '/'
	Config.data_dir = '/mnt/tatooine/data/'
	Config.speech_dir = Config.data_dir+'mode/wTIMIT/wav_speakers/'
	Config.test_speakers = test_speakers
	Config.noise_dir = Config.data_dir+'noise/noise_equal_concat/train/'
	Config.ir_dir = '/mnt/naboo/impulse_responses/'
	Config.ref_noise = Config.data_dir+'ref_pink.wav'

	Config.support_dir = Config.code_dir.replace('SourceCode','Datasets/Support')
	Config.noise_int_dir = Config.support_dir + 'noise_int/'
	Config.ir_int_dir = Config.support_dir + 'ir_int/'

	Config.working_dir = Config.code_dir.replace('SourceCode','Datasets/'+set)+ir_name+'/'
	audio_processing.makedirs(Config.working_dir)

	Config.ir_name = ir_name
	if (ir_name == 'rand_vs_lg_oben'):
		Config.test_ir = 'ir_lg_oben.wav'
		Config.test_pos = 'oben'
	elif (ir_name == 'rand_vs_s3_unten'):
		Config.test_ir = 'ir_s3_unten.wav'
		Config.test_pos = 'unten'
	elif (ir_name == 'rand_vs_s1_tisch'):
		Config.test_ir = 'ir_s1_tisch.wav'
		Config.test_pos = 'tisch'
	else:
		Config.test_ir = 'ir_htc_tasche.wav'
		Config.test_pos = 'tasche'

	Config.normalize_dir = Config.working_dir + 'normalized/'
	Config.noised_dir = Config.working_dir + 'cleanData/'
	Config.convolute_dir = Config.working_dir + 'channeledData/'
	Config.channeledData= 'channeledData'
	Config.cleanData= 'cleanData'

