__author__ = 'aiham'

####################################################
# this file transforms data in vad ".ano" files of #
# TIMIT corpora into the csv features files        #
####################################################
import csv
import glob
import os
import numpy as np

def adaptAnnotationsToFrameLength(ano_content, frames_count, frame_length, frame_inc):
	#adapt the annoation to the frame size. Average over the values
	binAnnotation = np.zeros((frames_count,1),dtype='int16')

	for i in range(frames_count):
		if(i == 0):
			binAnnotation[0] = sum(ano_content[1:frame_length])
			nextEndIndex = frame_length + frame_inc
		else:
			if(nextEndIndex > len(ano_content)):
				frame_length = frame_length - (nextEndIndex - len(ano_content))
				nextEndIndex = len(ano_content)

			binAnnotation[i] = sum(ano_content[nextEndIndex - frame_length + 0:nextEndIndex])
			nextEndIndex = nextEndIndex + frame_inc;

		try:
			# binAnnotation[i] =  round(binAnnotation[i] / frame_length)
			binAnnotation[i] =  binAnnotation[i] == frame_length
		except:
			binAnnotation[i] = 0
			pass

	return binAnnotation

# csv file load
def iter_load_csv(filename, delimiter=',', d_type=float):
	def iter_func():
		with open(filename, 'r') as infile:
			for line in infile:
				line = line.rstrip().split(delimiter)
				for item in line:
					yield d_type(item)
		iter_load_csv.row_length = len(line)

	data = np.fromiter(iter_func(), dtype=d_type)
	data = data.reshape((-1, iter_load_csv.row_length))
	return data

def filtr(files, filetype):
	"""Filters a file list by the given filename ending to be accepted"""
	return filter(lambda d: 1 if d.endswith(filetype) else 0, files)

def calc_append_labels(ir_name, where):

	vad_corpus_path = '/home/taleb/code/data_mixing/Datasets/vad/'+ir_name+'/'+where+'/'
	vad_ano_path = '/mnt/tatooine/data/vad_speaker_recog/TIMIT_Buckeye/vad/'

	frame_length_in_sec = 0.02
	frame_inc_in_sec = 0.01
	rate = 16000

	for subdir, dirs, files in os.walk(vad_ano_path):
		ano_files = filtr(files, 'ano')
		for fname in ano_files:
			ano_filename = subdir + '/' + fname

			subdirname = os.path.basename(os.path.dirname(ano_filename))

			csv_filenames = [f for f in glob.glob(vad_corpus_path+subdirname+'/*.csv') \
							if fname.replace('.ano','') in f and not '_withVAD.csv' in f]

			if (len(csv_filenames)>0):
				csv_filename = csv_filenames[0]
				print 'ano file: ' + ano_filename
				print 'csv file: ' + csv_filename

				ano_content = iter_load_csv(ano_filename, d_type=int)
				csv_content = iter_load_csv(csv_filename)

				adapted_ano = adaptAnnotationsToFrameLength(ano_content, len(csv_content), \
										frame_length_in_sec*rate, frame_inc_in_sec*rate)

				print 'Ano labels contain %.2f percent speech' % (100 * float(sum(ano_content))/len(ano_content))
				print 'Labels contain %.2f percent speech' % (100 * float(sum(adapted_ano))/len(adapted_ano))
				print '~~~~~#############################################~~~~~'

				both = []
				for ano_row, csv_row in zip(adapted_ano, csv_content):
					concat= np.hstack((csv_row,ano_row))
					both.append(np.ndarray.tolist(concat))

				output_filename = vad_corpus_path+'/'+subdirname+'/'+ \
								  os.path.basename(csv_filename).replace('.csv','')+'_withVAD.csv'
				with open(output_filename, 'w') as output:
					writer = csv.writer(output, delimiter=',')
					writer.writerows(both)

ir_name = 'none'
calc_append_labels(ir_name, 'cleanFeatures')

ir_name = 'rand_vs_lg_oben'
calc_append_labels(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_s3_unten'
calc_append_labels(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_s1_tisch'
calc_append_labels(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_htc_tasche'
calc_append_labels(ir_name, 'channeledFeatures')