import operator
from random import choice
from sklearn.neighbors import NearestNeighbors

__author__ = 'aiham'

# VAD classifier: trained from wav files in vad partition;
# Purpose: create VAD annotations for UBM and emotions
# This particular version processes a second set to allow for var. comparisons in classification
# (e.g. different impulse responses)

import numpy as np
import os
import csv
from sklearn.ensemble import RandomForestClassifier

# fast and lean csv load
def iter_load_csv(filename, delimiter=',', skiprows=0, dtype=float):
	def iter_func():
		with open(filename, 'r') as infile:
			for _ in range(skiprows):
				next(infile)
			for line in infile:
				if line.strip():
					line = line.rstrip().split(delimiter)
					for item in line:
						yield dtype(item)
					iter_load_csv.rowlength = len(line)

	data = np.fromiter(iter_func(), dtype=dtype)
	data = data.reshape((-1, iter_load_csv.rowlength))
	return data


def filtr(files, filetype):
	"""Filters a file list by the given filename ending to be accepted"""
	return filter(lambda d: 1 if d.endswith(filetype) else 0, files)



######
## balance data
######
def balance_dataset2(all_samples, all_targets):
	counts = np.bincount(np.array(all_targets, 'int'))
	print('Classes distributions before balancing')
	print(counts)
	majority_index, no_majority_samples = max(enumerate(counts), key=operator.itemgetter(1))
	for class_index in range(len(counts)):
		if class_index == majority_index: continue
		if (counts[class_index] > 0.9*float(no_majority_samples)):
			print('skipping emotion %d as it has enough samples.' % class_index)
			continue

		print("processing class: %d" % class_index)
		synthetic_size = no_majority_samples - counts[class_index]
		k = 20
		(synthetic_samples, danger_minorities) = \
			borderlineSMOTE(np.array(all_samples,'float32'), \
							all_targets, class_index, synthetic_size, k)
		print("done balancing class: %d" % class_index)

		random_synthetic_indices = np.random.randint(len(synthetic_samples), size=synthetic_size)
		all_samples = \
			np.append(all_samples, \
				  synthetic_samples[random_synthetic_indices], axis=0)
		all_targets = np.append(all_targets, \
				  np.full(len(synthetic_samples[random_synthetic_indices]),class_index), axis=0)

		counts[class_index] = len(all_samples)/2
	print('Classes distributions after balancing')
	print(counts)

	return all_samples,all_targets

def borderlineSMOTE(X, y, minority_target, synthetic_size, k):
	"""
	Returns synthetic minority samples.
	Parameters
	----------
	X : array-like, shape = [n__samples, n_features]: Holds the minority and majority samples
	y : array-like, shape = [n__samples]: Holds the class targets for samples
	minority_target : value for minority class
	synthetic_size : number of new synthetic samples
	k : int. Number of nearest neighbours.
	-------
	synthetic : Synthetic sample of minorities in danger zone
	danger : Minorities of danger zone
	"""
	y = np.array(y,'int8').flatten()

	print("applying SMOTE algorithm on minority samples...")

	#SMOTE danger minority samples
	synthetic_samples = SMOTE(X[y == minority_target], synthetic_size, k, h = 1.0)

	return (synthetic_samples, X[y == minority_target])

def SMOTE(T, synthetic_size, k, h = 1.0):
	"""
	Returns "synthetic_size" synthetic minority samples.
	Parameters
	----------
	T : array-like, shape = [n_minority_samples, n_features]: Holds the minority samples.
	synthetic_size : amount of new synthetic samples: n_synthetic_samples = synthetic_size
	k : int. Number of nearest neighbours.
	Returns
	-------
	S : Synthetic samples. array,
		shape = [synthetic_size, n_features].
	"""
	n_minority_samples, n_features = T.shape
	n_synthetic_samples = synthetic_size
	S = np.zeros(shape=(n_synthetic_samples, n_features))

	#Learn nearest neighbours
	neigh = NearestNeighbors(n_neighbors = k)
	neigh.fit(T)

	#Calculate synthetic samples
	for i in xrange(n_synthetic_samples):
		minority_index = np.random.randint(n_minority_samples)
		nn = neigh.kneighbors(T[minority_index], return_distance=False)
		nn_index = choice(nn[0])
		#NOTE: nn includes T[i], we don't want to select it
		while nn_index == minority_index:
			nn_index = choice(nn[0])
		dif = T[nn_index] - T[minority_index]
		gap = np.random.uniform(low = 0.0, high = h)
		S[i, :] = T[minority_index,:] + gap * dif[:]

	return S

def balance_dataset(unbalanced_samples, unbalanced_labels):
	counts = np.bincount(np.array(unbalanced_labels, 'int'))
	will_remain_indices = []
	balanced_labels_list = []
	balanced_samples_list = []

	if counts[0] < counts[1]:  # if zeros are less
		smaller = 0
		larger = 1
	else:
		smaller = 1
		larger = 0

	current_larger_counter = 0
	for index, value in enumerate(unbalanced_labels):
		if (len(will_remain_indices) == 2*counts[smaller]):
			break
		elif (value == smaller):
			will_remain_indices.append(index)
		else:
			if (value == larger):
				if (current_larger_counter < counts[smaller]):
					current_larger_counter += 1
					if (current_larger_counter % 10 == 0):
						print 'Balanced labels contain %.2f percent speech' % \
							  (100 * float(current_larger_counter) / (2*counts[smaller]))
					will_remain_indices.append(index)

	balanced_samples_list.extend(np.ndarray.tolist(unbalanced_samples[will_remain_indices]))
	balanced_labels_list.extend(unbalanced_labels[will_remain_indices])

	return (balanced_samples_list, np.array(balanced_labels_list, 'int'))

def train_vad_classifier(ir_name):
	print 'training vad for ir = ' + ir_name

	# configurations
	n_jobs = 50
	n_estimators = 50
	n_features = 36
	parameter_file_ext = 'm18w20i10.csv'

	# paths
	vad_path = '/home/taleb/data/corpora_mfcc_dd/' + ir_name +'/vad/'

	print("Reading VAD features and labels...")
	vad_training_features_anno = iter_load_csv(vad_path + parameter_file_ext)
	[vad_training_samples, vad_training_labels] = np.split(vad_training_features_anno, [n_features], 1)
	vad_training_labels = vad_training_labels.flatten()
	print("VAD read")

	print 'Labels before balancing contain %.2f percent speech' % (100 * float(sum(vad_training_labels)) / len(vad_training_labels))

	(balanced_samples, balanced_labels) = balance_dataset2(vad_training_samples, vad_training_labels)

	####################################
	### Random Forests
	### scikit-learn v0.15!!!!!
	####################################
	print("Fitting the random forest classifier on VAD data...")
	forest = RandomForestClassifier(n_estimators = n_estimators, criterion = 'entropy', min_samples_leaf = 1, \
	n_jobs = n_jobs)#, class_weight='auto')
	forest.fit(balanced_samples, balanced_labels)
	print("Fitting done")

	return forest

def annotate(test_speaker,ir_name,dataset,forest):
	print 'prediction for ir = ' + ir_name + ' and dataset = ' + dataset

	csvs_paths = '/home/taleb/data/corpora_mfcc_dd/' +ir_name+'/'+dataset+'/' \
				  +'all_vs_'+test_speaker+'/'

	n_features = 36

	all_files_samples = []
	all_files_paths = []

	# Reading features files and samples
	for subdir, dirs, files in os.walk(csvs_paths):
		if (subdir.find('vad') == -1 and subdir.find('ubm/by_file') == -1):
			# csvs = filtr(files, 'csv')
			csvs = filtr(files, 'm18w20i10.csv')
			for fname in csvs:
				if (fname.find("_annoVAD.csv") == -1):
					csv_filename = subdir + '/' + fname
					file_features = iter_load_csv(csv_filename)
					file_samples = file_features[:,:n_features]
					all_files_samples.append(np.array(file_samples))
					all_files_paths.append(csv_filename)
					print('file read: '+csv_filename)

	print("Samples reading done")

	# VAD prediction and writing the result in csv file
	if csvs_paths:
		print("Prediction of wavs VAD annotations, and writing the results...")
		for csv_file_samples_index in range(len(all_files_samples)):

			prediction_current_file = forest.predict(all_files_samples[csv_file_samples_index])

			[current_file_path,ext] = os.path.splitext(all_files_paths[csv_file_samples_index])
			with open(current_file_path + '_annoVAD.csv', 'wb') as csvfile:
				labelwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONE)
				prediction_emotion_file_int = prediction_current_file.astype(int)
				print 'predicted labels contain %.2f percent speech' % \
					  (100 * float(sum(prediction_emotion_file_int)) / len(prediction_emotion_file_int))
				prediction_emotion_file_str = prediction_emotion_file_int.astype(str)
				labelwriter.writerows(prediction_emotion_file_str)
				print('file VAD predicted: '+current_file_path)
		print("Prediction and writing the results done")

def annotate2(ir_name,dataset,forest):
	print 'prediction for ir = ' + ir_name + ' and dataset = ' + dataset

	csvs_paths = '/home/taleb/data/corpora_mfcc_dd/' +ir_name+'/'+dataset+'/'

	n_features = 36

	all_files_samples = []
	all_files_paths = []

	# Reading features files and samples
	for subdir, dirs, files in os.walk(csvs_paths):
		if (subdir.find('vad') == -1 and subdir.find('ubm/by_file') == -1):
			# csvs = filtr(files, 'csv')
			csvs = filtr(files, 'm18w20i10.csv')
			for fname in csvs:
				if (fname.find("_annoVAD.csv") == -1):
					csv_filename = subdir + '/' + fname
					file_features = iter_load_csv(csv_filename)
					file_samples = file_features[:,:n_features]
					all_files_samples.append(np.array(file_samples))
					all_files_paths.append(csv_filename)
					print('file read: '+csv_filename)

	print("Samples reading done")

	# VAD prediction and writing the result in csv file
	if csvs_paths:
		print("Prediction of wavs VAD annotations, and writing the results...")
		for csv_file_samples_index in range(len(all_files_samples)):

			prediction_current_file = forest.predict(all_files_samples[csv_file_samples_index])

			[current_file_path,ext] = os.path.splitext(all_files_paths[csv_file_samples_index])
			with open(current_file_path + '_annoVAD.csv', 'wb') as csvfile:
				labelwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONE)
				prediction_emotion_file_int = prediction_current_file.astype(int)
				print 'predicted labels contain %.2f percent speech' % \
					  (100 * float(sum(prediction_emotion_file_int)) / len(prediction_emotion_file_int))
				prediction_emotion_file_str = prediction_emotion_file_int.astype(str)
				labelwriter.writerows(prediction_emotion_file_str)
				print('file VAD predicted: '+current_file_path)
		print("Prediction and writing the results done")



Semaine_test_speakers = ['Spike','Prudence','Poppy']
FAU_test_speakers = ['Mont_07','Mont_08','Ohm_18']
ir_names = ['none','rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche']

for ir_name in ir_names:
	forest = train_vad_classifier(ir_name)
	dataset = 'ubm'
	annotate2(ir_name,dataset,forest)
	# for test_speaker in FAU_test_speakers:
	# 	dataset = 'FAU'
	# 	annotate(test_speaker,ir_name,dataset,forest)
	#
	# for test_speaker in Semaine_test_speakers:
	# 	dataset = 'Semaine'
	# 	annotate(test_speaker,ir_name,dataset,forest)



# ir_name = 'none'
# forest = train_vad_classifier(ir_name)
#
# # dataset = 'FAU'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'Semaine'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'ubm'
# # annotate(ir_name,dataset,forest)
#
# dataset = 'voxforge'
# annotate(ir_name,dataset,forest)
#
#
#
#
# ir_name = 'rand_vs_lg_oben'
# forest = train_vad_classifier(ir_name)
#
# # dataset = 'FAU'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'Semaine'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'ubm'
# # annotate(ir_name,dataset,forest)
#
# dataset = 'voxforge'
# annotate(ir_name,dataset,forest)
#
#
#
#
# ir_name = 'rand_vs_s3_unten'
# forest = train_vad_classifier(ir_name)
#
# # dataset = 'FAU'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'Semaine'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'ubm'
# # annotate(ir_name,dataset,forest)
#
# dataset = 'voxforge'
# annotate(ir_name,dataset,forest)
#
#
#
#
# ir_name = 'rand_vs_s1_tisch'
# forest = train_vad_classifier(ir_name)
#
# # dataset = 'FAU'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'Semaine'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'ubm'
# # annotate(ir_name,dataset,forest)
#
# dataset = 'voxforge'
# annotate(ir_name,dataset,forest)
#
#
#
#
# ir_name = 'rand_vs_htc_tasche'
# forest = train_vad_classifier(ir_name)
#
# # dataset = 'FAU'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'Semaine'
# # annotate(ir_name,dataset,forest)
# #
# # dataset = 'ubm'
# # annotate(ir_name,dataset,forest)
#
# # dataset = 'voxforge'
# # annotate(ir_name,dataset,forest)