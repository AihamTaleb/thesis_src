__author__ = 'aiham'
#############################################################
### code for copying multiple csv files into one big file ###
#############################################################

import os

def filtr(files, name_ending):
	"""Filters a file list by the given filename ending to be accepted"""
	return filter(lambda d: 1 if d.endswith(name_ending) else 0, files)

def consolidate_vad_files(ir_name, where):
	dataset = 'vad'

	samples_numb_features = 18
	frame_length = 20
	frame_inc = 10
	parameter_file_config_samples = 'm' + str(samples_numb_features) + 'w' \
									+ str(frame_length) + 'i' + str(frame_inc)

	# input files path
	corpus_path = '/home/taleb/code/data_mixing/Datasets/vad/'+ir_name+'/'+where+'/'

	# output filename
	outfilename = '/home/taleb/data/corpora_mfcc_dd/' + ir_name +'/'+ dataset +'/'+ \
				  parameter_file_config_samples + '.csv'

	with open(outfilename, 'w+') as outfile:
		# copying input files contents to the output file
		for subdir, dirs, files in os.walk(corpus_path):
			features_files = filtr(files, '_withVAD.csv')
			for fname in features_files:
				feature_file = subdir + '/' + fname
				print 'feature file: ' + feature_file
				with open(feature_file, 'r') as readfile:
					for line in readfile:
						outfile.write(line)

					if (line.strip()):
						outfile.write('\n')

ir_name = 'none'
consolidate_vad_files(ir_name, 'cleanFeatures')

ir_name = 'rand_vs_lg_oben'
consolidate_vad_files(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_s3_unten'
consolidate_vad_files(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_s1_tisch'
consolidate_vad_files(ir_name, 'channeledFeatures')

ir_name = 'rand_vs_htc_tasche'
consolidate_vad_files(ir_name, 'channeledFeatures')
