"""
This script is the starting point for training i-vectors (total variability)
to perform the emotion recognition task
"""
__author__ = 'aiham'

# can be either 'Semaine' or 'FAU'
emotion_corpus_name = 'Semaine'

# specifying the test speakers
if (emotion_corpus_name == 'FAU'):
    test_speakers = ['Mont_07','Mont_08','Ohm_18','Ohm_10']
else:
    test_speakers = ['Spike','Prudence','Poppy','Obadiah']

## base results path
base_results_path = '/home/taleb/results/'
## base features path
base_path_features = '/home/taleb/data/corpora_mfcc_dd/'

## create results folder (to store the trained ubm model and evaluation results)
from auxiliary_functions import create_results_folder
results_path = create_results_folder(base_results_path)

######################
### Configurations ###
######################
norm_type = 'normcos'
frame_length = 20
frame_inc = 10
num_features = 36
k_fold = 10
n_jobs_kmeans = 10
num_mixtures_ubm = 256
gmm_covtype = 'diag'
num_iter_em = 50
frames_per_utterance = 256 #256 roughly equals 2.5s
ivector_dim = 400
lda_subspace_dim = 200
sampling_rate = 16000
balance_data = False

# 0: on CPU
# 1: on GPU
ubm_training_mode = 1

# 0: no additional debug infos
# 1: gpu stats
# 2: detailed scoring; detailed runtimes
debug_level = 2

#################
### filenames ###
#################
#samples configs
samples_numb_features = 18
parameter_file_config_samples = 'm' + str(samples_numb_features) + 'w' \
	+ str(frame_length) + 'i' + str(frame_inc)

vad_label_extension = '_annoVAD'

#############################
### Training & Evaluation ###
#############################
metrics_list = []
clean_metrics_list = []

# ubm_to_use = 'voxforge'
ubm_to_use = 'ubm'

from gmm_tv_cosine_scoring_no_wccn import gmm_tv

for test_speaker in test_speakers:
    print('******* Testing against speaker %s *******' % test_speaker)

    ## Should we load pretrained models? If not set these strings to []
    ubm_model = '/home/taleb/results/TV/ubms/timit/ubm_'+str(num_mixtures_ubm)+'_s1126400_rand_vs_lg_oben.hdf5'
    ivec_machine_model = []
    white_machine_model = []
    lda_machine_model = []

    ir_name = 'rand_vs_lg_oben'
    ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
    emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
    test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'
    emotion_by_file_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/by_file/'

    settings = 	(base_path_features, results_path, ubm_model, ivec_machine_model, \
        white_machine_model, lda_machine_model, norm_type, frame_inc, sampling_rate, \
        k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
        frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
        vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, \
        test_speaker_path, ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data)
    result = gmm_tv(settings)
    metrics_list.append(result)


    ## Should we load pretrained models? If not set these strings to []
    ubm_model = '/home/taleb/results/TV/ubms/timit/ubm_'+str(num_mixtures_ubm)+'_s1133056_rand_vs_s3_unten.hdf5'
    ivec_machine_model = []
    white_machine_model = []
    lda_machine_model = []

    ir_name = 'rand_vs_s3_unten'
    ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
    emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
    test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'
    emotion_by_file_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/by_file/'

    settings = 	(base_path_features, results_path, ubm_model, ivec_machine_model, \
        white_machine_model, lda_machine_model, norm_type, frame_inc, sampling_rate, \
        k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
        frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
        vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, \
        test_speaker_path, ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data)
    result = gmm_tv(settings)
    metrics_list.append(result)


    ## Should we load pretrained models? If not set these strings to []
    ubm_model = '/home/taleb/results/TV/ubms/timit/ubm_'+str(num_mixtures_ubm)+'_s1117696_rand_vs_s1_tisch.hdf5'
    ivec_machine_model = []
    white_machine_model = []
    lda_machine_model = []

    ir_name = 'rand_vs_s1_tisch'
    ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
    emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
    test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'
    emotion_by_file_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/by_file/'

    settings = 	(base_path_features, results_path, ubm_model, ivec_machine_model, \
        white_machine_model, lda_machine_model, norm_type, frame_inc, sampling_rate, \
        k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
        frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
        vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, \
        test_speaker_path, ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data)
    result = gmm_tv(settings)
    metrics_list.append(result)

    ## Should we load pretrained models? If not set these strings to []
    ubm_model = '/home/taleb/results/TV/ubms/timit/ubm_'+str(num_mixtures_ubm)+'_s1113600_rand_vs_htc_tasche.hdf5'
    ivec_machine_model = []
    white_machine_model = []
    lda_machine_model = []

    ir_name = 'rand_vs_htc_tasche'
    ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
    emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
    test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'
    emotion_by_file_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/by_file/'

    settings = 	(base_path_features, results_path, ubm_model, ivec_machine_model, \
        white_machine_model, lda_machine_model, norm_type, frame_inc, sampling_rate, \
        k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
        frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
        vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, \
        test_speaker_path, ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data)
    result = gmm_tv(settings)
    metrics_list.append(result)

    ## Should we load pretrained models? If not set these strings to []
    ubm_model = '/home/taleb/results/TV/ubms/timit/ubm_'+str(num_mixtures_ubm)+'_s1237248_none.hdf5'
    ivec_machine_model = []
    white_machine_model = []
    lda_machine_model = []

    ir_name = 'none'
    ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
    emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
    test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'
    emotion_by_file_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/by_file/'

    settings = 	(base_path_features, results_path, ubm_model, ivec_machine_model, \
        white_machine_model, lda_machine_model, norm_type, frame_inc, sampling_rate, \
        k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
        frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
        vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, \
        test_speaker_path, ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data)
    result = gmm_tv(settings)
    clean_metrics_list.append(result)

    print('******* Done testing against speaker %s *******' % test_speaker)


from auxiliary_functions import calc_overall_performance
(clean_unweighted_avg_recall, clean_avg_recalls_per_emotions, clean_overall_confusion_matrix, \
	clean_overall_unweighted_avg_precision, clean_avg_precisions_per_emotions, \
    clean_overall_unweighted_avg_f1, clean_avg_f1s_per_emotions, clean_overall_acc) = \
    calc_overall_performance(clean_metrics_list)

print '****************************************************************************************'
print 'Evaluation on %s corpus (No-IRs) is done' % (emotion_corpus_name)
print 'Final unweighted average recall over all emotions (clean) is %.2f' % (clean_unweighted_avg_recall)
print 'Final unweighted average recall for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_recalls_per_emotions]))
print 'Final unweighted average precision over all emotions (clean) %.2f' % (clean_overall_unweighted_avg_precision)
print 'Final unweighted average precision for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_precisions_per_emotions]))
print 'Final unweighted average f1 over all emotions (clean) is %.2f' % (clean_overall_unweighted_avg_f1)
print 'Final unweighted average f1 for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_f1s_per_emotions]))
print 'Final accuracy over all emotions (clean) is %.2f' % (clean_overall_acc)
print 'Final overall confusion matrix over emotions (clean) is : '
print (clean_overall_confusion_matrix)
print '****************************************************************************************'


(overall_unweighted_avg_recall, avg_recalls_per_emotions, overall_confusion_matrix, \
	overall_unweighted_avg_precision, avg_precisions_per_emotions, overall_unweighted_avg_f1, \
	avg_f1s_per_emotions, overall_acc) = calc_overall_performance(metrics_list)

print '****************************************************************************************'
print 'Evaluation on %s corpus (with-IRs) is done' % (emotion_corpus_name)
print 'Final unweighted average recall over all emotions and sets is %.2f' % (overall_unweighted_avg_recall)
print 'Final unweighted average recall for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_recalls_per_emotions]))
print 'Final unweighted average precision over all emotions and sets is %.2f' % (overall_unweighted_avg_precision)
print 'Final unweighted average precision for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_precisions_per_emotions]))
print 'Final unweighted average f1 over all emotions and sets is %.2f' % (overall_unweighted_avg_f1)
print 'Final unweighted average f1 for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_f1s_per_emotions]))
print 'Final accuracy over all emotions and sets is %.2f' % (overall_acc)
print 'Final overall confusion matrix over all sets and emotions is : '
print (overall_confusion_matrix)
print '****************************************************************************************'