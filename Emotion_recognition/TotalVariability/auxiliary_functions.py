"""
This script is a library of helper functions
"""
__author__ = 'aihamtaleb'

import bob
import numpy as np
import random
import time
import csv
import os
import datetime

from gmm_specializer.gmm import GMM
from sklearn import metrics
from sklearn.mixture import GMM as ScipyGMM
from sklearn.cluster import MiniBatchKMeans, KMeans
from itertools import izip_longest

def train_ubm_bob(ubm_settings):
	(num_mixtures_ubm, num_dimension, gmm_covtype, n_jobs_kmeans, training_samples_ubm,
		num_iter_em, debug_level, ir_name, results_path, ubm_training_mode, emotion_corpus_name,
		frames_per_utterance) = ubm_settings

	training_samples_ubm = normalize_features_window(training_samples_ubm, len(training_samples_ubm),
						 num_dimension, frames_per_utterance)

	if ubm_training_mode == 0: # on CPU
		gmm_fast_ubm = train_ubm_cpu(num_mixtures_ubm, gmm_covtype, training_samples_ubm, num_iter_em)

		ubm = bob.machine.GMMMachine(num_mixtures_ubm, num_dimension)
		ubm.means = np.array(gmm_fast_ubm.means_, 'float64')
		ubm.weights = np.array(gmm_fast_ubm.weights_, 'float64')
		ubm.variances =  np.array(gmm_fast_ubm.covars_, 'float64')

	else: # on GPU
		gmm_fast_ubm = train_ubm_cuda(num_mixtures_ubm, num_dimension, gmm_covtype, n_jobs_kmeans, \
									  training_samples_ubm, num_iter_em, debug_level)
		ubm = bob.machine.GMMMachine(num_mixtures_ubm, num_dimension)
		ubm.means = np.array(gmm_fast_ubm.get_all_component_means(), 'float64')
		ubm.weights = np.array(gmm_fast_ubm.get_all_component_weights(), 'float64')
		ubm.variances = np.array(gmm_fast_ubm.get_all_component_full_covariance(), \
			'float64').diagonal(axis1=1,axis2=2)

	gmm_fast_ubm = []

	ubm_file = 'ubm_' + str(num_mixtures_ubm) + '_s' + str(len(training_samples_ubm)) + \
			   '_' + ir_name +'.hdf5'
	ubm.save(bob.io.HDF5File(results_path + ubm_file, 'w' ))
	print 'UBM trained model saved to path: ' + results_path + ubm_file

	return ubm

def train_ubm_cpu(num_mixtures_ubm, gmm_covtype, training_samples_ubm, num_iter_em):

	print 'Starting UBM training on the CPU...'

	tic = time.time()
	print 'Init K-Means for UBM-GMM means ...'
	initKMeans = KMeans(n_clusters=num_mixtures_ubm, init='k-means++', n_init=1, max_iter=10, \
		verbose=0, random_state=1)
	initKMeans.fit(training_samples_ubm)
	print 'Finished init K-Means for UBM-GMM means.'

	print 'UBM Training...'
	gmm_fast_ubm = ScipyGMM(n_components=num_mixtures_ubm, covariance_type=gmm_covtype, \
					   n_iter=num_iter_em, init_params='wc')
	#cluster centers are mean values
	gmm_fast_ubm.means_ = initKMeans.cluster_centers_
	gmm_fast_ubm.fit(training_samples_ubm)
	toc = time.time()
	print 'UBM training done. Time passed: %fs' % (toc-tic)

	return gmm_fast_ubm


#kmeans to initialize the parameters of the gmm
def init_with_kmeans(num_mixtures, num_dimension, n_jobs_kmeans, samples_fold):
	print 'k-means init on the CPU...'
	initKMeans = KMeans(n_clusters=num_mixtures, init='k-means++', n_init=1, max_iter=5, \
		verbose=0, random_state=1, n_jobs=n_jobs_kmeans)
	initKMeans.fit(samples_fold)
	#cluster center are mean values
	init_mean = initKMeans.cluster_centers_

	#calc covariance matrix for each cluster/mixture
	init_cov = np.zeros((num_mixtures,num_dimension,num_dimension))
	for i in range(num_mixtures):
		cluster_index = i
		samples_of_cluster = samples_fold[initKMeans.labels_ == cluster_index]
		if len(samples_of_cluster) > 0:
			cov_of_cluster = np.cov(samples_of_cluster.T)
			diag_cov = cov_of_cluster * np.identity(num_dimension)
		else:
			diag_cov = np.identity(num_dimension)
		init_cov[i,:,:] = diag_cov

	init_mean = np.array(init_mean,dtype=np.float32)
	init_cov =  np.array(init_cov,dtype=np.float32)

	print 'Done k-means init on the CPU.'

	return init_mean, init_cov


def train_ubm_cuda(num_mixtures_ubm, num_dimension, gmm_covtype, n_jobs_kmeans, training_samples_ubm, \
	num_iter_em, debug_level):

	init_mean_ubm, init_cov_ubm = init_with_kmeans(num_mixtures_ubm, num_dimension, \
		n_jobs_kmeans, training_samples_ubm)
	init_weight_ubm = np.ones(num_mixtures_ubm)
	init_weight_ubm = init_weight_ubm * (1/np.float32(num_mixtures_ubm))
	init_weight_ubm = np.array(init_weight_ubm,dtype=np.float32)

	print 'Starting UBM training on the GPU...'
	tic = time.time()
	gmm_fast_ubm = GMM(num_mixtures_ubm, num_dimension, cvtype=gmm_covtype, means=init_mean_ubm, \
		covars=init_cov_ubm, weights=init_weight_ubm)
	gmm_fast_ubm.train(training_samples_ubm,max_em_iters=num_iter_em, min_em_iters=num_iter_em)
	toc = time.time()
	print 'UBM training done. Time passed: %fs' % (toc-tic)

	return gmm_fast_ubm

def prepare_eval_data(ts_emotions_samples_voiced_eval, all_test_speaker_data_partitions, \
							   no_of_emotions, cross_index):
	#build test data (one single test vector)
	test_chunks_all_emotions = []

	for emotion_index_for_test in range(no_of_emotions):
		#get test data of emotion
		ts_emotion_test_partitions = all_test_speaker_data_partitions[emotion_index_for_test]
		test_partition_index = ts_emotion_test_partitions[cross_index]
		emotion_samples = ts_emotions_samples_voiced_eval[emotion_index_for_test]
		test_samples = np.float32(emotion_samples[test_partition_index]).tolist()

		#cut test data into more realistic utterance-like chunks
		while test_samples:
			#randomly pick size of next chunk from range 400 to 2000 frames.
			#for frame_length=20 and increment=10 this amounts to roughly 4-20 secs.
			chunk_size = random.randint(400, 2000)
			#don't get bigger chunk than number of elements in the list
			if chunk_size >= len(test_samples):
				test_chunks_all_emotions.append((test_samples,np.full(len(test_samples), emotion_index_for_test)))
				test_samples = []
			else:
				chunk = []
				#get sublist: pop first element, chunk_size times
				for _ in range(chunk_size):
					chunk.append(test_samples.pop(0))
				test_chunks_all_emotions.append((chunk,np.full(chunk_size, emotion_index_for_test)))

	random.shuffle(test_chunks_all_emotions)

	#build lists from tuples and flatten, e.g. [[0,0,0],[1,1,1]] -> [0,0,0,1,1,1]
	test_samples = [item for (samp,lab) in test_chunks_all_emotions for item in samp]
	test_labels = [item for (samp,lab) in test_chunks_all_emotions for item in lab]

	return (test_samples, test_labels)


def create_results_folder(base_results_path):
	# if the folder already exists, append counter suffix
	date_str = str(datetime.date.today())
	results_path = base_results_path + 'TV/' + date_str
	if  os.path.exists(results_path):
		counter = 2
		while os.path.exists(results_path + '_' + str(counter)):
			counter = counter + 1
		results_path = results_path + '_' + str(counter) + '/'
	else:
		results_path = results_path + '/'
	os.makedirs(results_path)
	print 'The designated result path for this run is %s.' % (results_path)
	return results_path


def project_features_ubm(all_emotions_by_file_samples_voiced, frames_per_utterance, ubm):
	#Project features (compute sufficient stats of features against UBM = first step of MAP)
	#This reuses features that have been used for training the UBM

	proj_train_features = []

	print 'Projecting all emotion files samples...'
	for emotion_files_samples in all_emotions_by_file_samples_voiced:

		for file_samples in emotion_files_samples:
			proj_train_features_emotion_file = []

			#We can either create one GMM per impostor or one GMM per impostor
			#utterance. The latter allows for more emotion and channel factors.
			if len(file_samples) > frames_per_utterance:
				num_utt = int(round(len(file_samples)/frames_per_utterance))
				for utt_count in range(0, num_utt):
					data = file_samples[utt_count*frames_per_utterance \
						: (utt_count+1)*frames_per_utterance]

					data = norm_utt(data)

					proj_data = bob.machine.GMMStats(ubm.shape[0], ubm.shape[1])
					ubm.acc_statistics(data, proj_data)
					proj_train_features_emotion_file.append(proj_data)
				proj_train_features.append(proj_train_features_emotion_file)
			else:
				file_samples = normalize_features_window(file_samples, len(file_samples),
								 len(file_samples[0]),frames_per_utterance)
				proj_train_features_emotion_file = bob.machine.GMMStats(ubm.shape[0], ubm.shape[1])
				ubm.acc_statistics(file_samples, proj_train_features_emotion_file)
				proj_train_features.append([proj_train_features_emotion_file])

	print 'Done projecting.'
	return proj_train_features


##########
### normalize data
##########
def l2_norm(voiced_data):
	try:
		tmp_std = np.std(voiced_data, 0)
		voiced_data -= np.mean(voiced_data, 0)
		voiced_data /= tmp_std
		return voiced_data
	except:
		voiced_data -= np.mean(voiced_data, 0)
		return voiced_data

def norm_utt(utt_data):
	try:
		tmp_std = np.std(utt_data, 0)
		utt_data -= np.mean(utt_data, 0)
		utt_data /= tmp_std
		return utt_data
	except:
		utt_data -= np.mean(utt_data, 0)
		return utt_data

def norm(a_i, b_i, c_i):
	try:
		return (a_i-b_i)/c_i
	except:
		return (a_i-b_i)

def normalize_features_window(voiced_data, frames_count, num_features, \
							  frames_per_utterance, frame_norm_inc = 128):
	np.seterr(all='raise')
	# voiced_data = l2_norm(voiced_data)
	if (len(voiced_data) > frames_per_utterance):
		# split data into utterances and normalize each
		num_utt = int(round(len(voiced_data)/frames_per_utterance))
		for utt_count in range(0, num_utt):
			#get values from array, normalize, then update array
			utt_data = voiced_data[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance]
			utt_data = norm_utt(utt_data)
			voiced_data[utt_count*frames_per_utterance \
				: (utt_count+1)*frames_per_utterance] = utt_data

		#ignore remaining frames that don't fill the last window
		voiced_data = voiced_data[:num_utt*frames_per_utterance]
	else:
		# normalize features locally for each frame: by subtracting the mean of its
		# surrounding neighbors in a radius of "frame_norm_inc". And dividing by their std.
		original_data = np.copy(voiced_data)
		begin_index = 0
		for i in range(frames_count):
			begin_index = min(frame_norm_inc, i-begin_index)
			end_index = min(frame_norm_inc, len(original_data)-i)
			neighbors_features_means = np.nanmean(original_data[i-begin_index:i+end_index, :num_features], axis=0)
			neighbors_features_stds = np.nanstd(original_data[i-begin_index:i+end_index, :num_features], axis=0)

			voiced_data[i,:num_features] = [norm(a_i,b_i,c_i) for a_i, b_i, c_i in \
									 zip(original_data[i,:num_features], neighbors_features_means, neighbors_features_stds)]

	return voiced_data


def calc_overall_performance(metrics_list):

	# recall
	emotion_recalls_list = [a for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_recalls_per_emotions = np.nanmean(emotion_recalls_list, axis=0)
	overall_unweighted_avg_recall = np.nanmean([b for (a,b,c,d,e,f,g,h) in metrics_list])

	# precision
	emotion_precisions_list = [e for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_precisions_per_emotions = np.nanmean(emotion_precisions_list, axis=0)
	overall_unweighted_avg_precision = np.nanmean([f for (a,b,c,d,e,f,g,h) in metrics_list])

	# f1
	emotion_f1s_list = [g for (a,b,c,d,e,f,g,h) in metrics_list]
	avg_f1s_per_emotions = np.nanmean(emotion_f1s_list, axis=0)
	overall_unweighted_avg_f1 = np.nanmean([h for (a,b,c,d,e,f,g,h) in metrics_list])

	# accuracy
	overall_acc = np.nanmean([d for (a,b,c,d,e,f,g,h) in metrics_list])

	# confusion matrix
	overall_confusion_matrix = np.sum([c for (a,b,c,d,e,f,g,h) in metrics_list], axis=0)

	return (overall_unweighted_avg_recall, avg_recalls_per_emotions, overall_confusion_matrix, \
			overall_unweighted_avg_precision, avg_precisions_per_emotions, overall_unweighted_avg_f1, \
			avg_f1s_per_emotions, overall_acc)