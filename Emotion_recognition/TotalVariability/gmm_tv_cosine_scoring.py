"""
This script trains i-vectors (total variability) to perform
the emotion recognition task, and it uses cosine scoring to calculate
the similarity between two i-vectors
"""
__author__ = 'aiham'

### Training
# 1. Trains a Universal Background Model (UBM GMM)
# 2. MAP-adapts the UBM for each UBM emotion (= projection)
# 3. Uses UBM emotion GMMs (supervectors) to estimate the global TV space
# 4. Projects supervectors into TV space -> I-vectors
# 4. Uses I-vectors to train whitening (decorrelation)
# 5. Whitens I-vectors
# 6. Uses whitened I-vectors to create LDA projection matrix
# 7. LDA-projects whitened I-vectors
# 8. Uses LDA-I-Vectors to train WCCN projection matrix
# 9. WCCN-projects LDA-I-Vectors -> final vector form

### Personal model enrollment & scoring:
## Either compare WCCN-LDA-I-vectors directly using cosine similarity:
# 1. Project training data of emotion using the above projections
#    (GMM -> I-Vector -> LDA -> WCCN) which gives us the personal
#    emotion I-vector (just one)
# 2. Project each test utterance to an I-vector and compare the
#    utterance I-vector to the personal emotion I-vector -> cosine
#    similarity score
## OR
# 1. Estimate common PLDA base using UBM WCCN-LDA-I-vectors
# 2. Enroll emotion data to get personal emotion PLDA model
# 3. Project each test utterance to an I-vector and classify it
#    using the PLDA classifier -> score

########################################################################

import bob
import numpy as np
from itertools import izip
from math import sqrt
import time

def gmm_tv(settings):
	(base_path_features, results_path, ubm_model, ivec_machine_model, \
	white_machine_model, lda_machine_model, wccn_machine_model, norm_type, frame_inc, \
	sampling_rate, k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, \
	num_iter_em, frames_per_utterance, ivector_dim, lda_subspace_dim, parameter_file_config_samples, \
	vad_label_extension, ubm_corpus_path, emotion_by_file_path, emotions_path, test_speaker_path, \
	ir_name, debug_level, ubm_training_mode, emotion_corpus_name, balance_data) = settings

	#################
	### read data ###
	#################
	from read_data import read_ubm_and_emotions
	settings_data = (base_path_features, ubm_corpus_path, emotions_path, ubm_model, test_speaker_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, vad_label_extension,\
		frames_per_utterance, balance_data)
	# ubm, emotion training data, and test data from one speaker.
	(ubm_training_samples_voiced, all_emotions_samples_voiced, \
		all_test_speaker_samples_voiced, all_emotions_training_partitions, \
		all_test_speaker_data_partitions) = read_ubm_and_emotions(settings_data)

	from read_data import read_emotions_by_file
	settings_data = (base_path_features, emotion_by_file_path, num_features, \
		parameter_file_config_samples, vad_label_extension, frames_per_utterance)
	all_emotions_by_file_samples_voiced = read_emotions_by_file(settings_data)

	no_of_emotions = len(all_emotions_samples_voiced)
	num_dimension = num_features

	#####################
	### train the UBM ###
	#####################
	if ubm_model:
		ubm = bob.machine.GMMMachine(bob.io.HDF5File(ubm_model, 'r'))
		print 'UBM loaded.'
	else:
		training_samples_ubm = np.float32(ubm_training_samples_voiced)
		from auxiliary_functions import train_ubm_bob
		ubm_settings = (num_mixtures_ubm, num_dimension, gmm_covtype, n_jobs_kmeans, training_samples_ubm, \
		num_iter_em, debug_level, ir_name, results_path, ubm_training_mode, emotion_corpus_name)
		ubm = train_ubm_bob(ubm_settings)

	ubm_training_samples_voiced = []

	###########################
	### features projection ###
	###########################
	from auxiliary_functions import project_features_ubm
	proj_train_features = project_features_ubm(all_emotions_by_file_samples_voiced, \
											   frames_per_utterance, ubm)

	proj_train_features_flat = [item for sublist in proj_train_features for item in sublist]

	##########################################
	### i-vector & whitening machines init ###
	##########################################
	ivec_machine = init_ivector_machine(ivec_machine_model, ubm, ivector_dim, proj_train_features_flat, \
		ir_name, results_path, debug_level, emotion_corpus_name)

	whitening_machine = init_whitening_machine(white_machine_model, ivec_machine, \
		proj_train_features_flat, ir_name, results_path, debug_level, emotion_corpus_name)

	print 'Starting I-Vector whitening and normalization...'
	whitened_ivectors = []
	for projected_features_emotions_by_file in proj_train_features:
		emotion_ivectors = []
		for gmmstats in projected_features_emotions_by_file:
			projected_ivector = ivec_machine.forward(gmmstats)
			whitened_ivector = whitening_machine.forward(projected_ivector)
			normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
			emotion_ivectors.append(normalized_ivector)
		whitened_ivectors.append(np.vstack(emotion_ivectors))
	print 'I-Vector whitening and normalization done.'

	# projected features after whitening
	proj_train_features = whitened_ivectors

	#####################################
	### LDA machine init & projection ###
	#####################################
	lda_machine = init_lda_machine(lda_machine_model, proj_train_features, lda_subspace_dim, \
		ir_name, results_path, debug_level, emotion_corpus_name)

	print 'Starting LDA projection...'
	lda_projected_ivectors = []
	for whitened_ivectors_emotion in proj_train_features:
		emotion_lda_ivectors = []
		for ivector in whitened_ivectors_emotion:
			lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
			lda_machine(ivector, lda_ivector)
			emotion_lda_ivectors.append(lda_ivector)
		lda_projected_ivectors.append(np.vstack(emotion_lda_ivectors))
	print 'LDA projection done.'

	# projected features after LDA projection
	proj_train_features = lda_projected_ivectors

	######################################
	### WCCN machine init & projection ###
	######################################
	wccn_machine = init_wccn_machine(wccn_machine_model, proj_train_features, ir_name, \
		results_path, debug_level, emotion_corpus_name)

	# imposters data preparation
	impostor_ivectors = get_impostor_emotions_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, \
		wccn_machine, all_emotions_by_file_samples_voiced)

	# function prepare_eval_data uses test_speaker data only
	# test_speaker data are separated into emotion folders.
	imp_sqrt_diag_covar_mat = np.sqrt(np.diag(np.cov(np.matrix(impostor_ivectors).T).diagonal()))
	# should be ivecdim x ivecdim
	print 'Shape of impostor_sqrt_diag_covar_mat is (%d, %d)' % (imp_sqrt_diag_covar_mat.shape)
	mean_impostor_ivector = np.mean(impostor_ivectors, 0)

	###############################
	### K-Fold cross validation ###
	###############################
	all_predictions = []
	all_labels = []
	for cross_index in range(k_fold):
		print('processing fold number %d:' % cross_index)
		emotion_specific_ivectors = []
		print 'Personal enrollment of emotions.'
		ts_0 = time.time()
		for emotion_index in range(no_of_emotions):
			# build training data emotion
			emotion_training_partitions = all_emotions_training_partitions[emotion_index]
			training_partition_index = emotion_training_partitions[cross_index]
			emotion_samples = all_emotions_samples_voiced[emotion_index]
			emotion_training_samples = np.float64(emotion_samples[training_partition_index])

			if len(emotion_training_samples) > frames_per_utterance:
				emotion_ivectors = []
				num_utt = int(round(len(emotion_training_samples)/frames_per_utterance))
				for utt_count in range(0, num_utt):
					data = emotion_training_samples[utt_count*frames_per_utterance \
						: (utt_count+1)*frames_per_utterance]
					utt_ivector = compute_compensated_ivector(data, ubm, ivec_machine, whitening_machine, \
															  lda_machine, wccn_machine)
					emotion_ivectors.append(utt_ivector)
				emotion_lda_ivector = np.nanmean(np.array(emotion_ivectors),axis=0)
			else:
				emotion_lda_ivector = compute_compensated_ivector(emotion_training_samples, \
					ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine)
			emotion_specific_ivectors.append(emotion_lda_ivector)
		ts_1 = time.time()
		print 'Emotions enrollment done. Time passed: %fs' % (ts_1-ts_0)

		##################
		### Evaluation ###
		##################
		print 'Evaluation of emotions ivectors over this fold\'s utterances...'
		from auxiliary_functions import prepare_eval_data
		# preparing test samples and labels
		ts_emotions_samples_voiced_eval = all_test_speaker_samples_voiced
		(test_samples, test_labels) = \
			prepare_eval_data(ts_emotions_samples_voiced_eval, all_test_speaker_data_partitions, \
												   no_of_emotions, cross_index)
		test_samples = np.float64(test_samples)
		test_labels = np.int8(test_labels)

		# get predictions and store scores for samples in-place
		(fold_predictions, fold_labels) = make_predictions_tv(ubm, ivec_machine, whitening_machine, lda_machine, \
				wccn_machine, test_samples, test_labels, frames_per_utterance, norm_type, \
				imp_sqrt_diag_covar_mat, mean_impostor_ivector, emotion_specific_ivectors, \
				debug_level, no_of_emotions)
		all_labels.extend(fold_labels)
		all_predictions.extend(fold_predictions)
		print('done processing fold number %d' % cross_index)

	print 'Evaluation of this set is done. Calculating metrics...'
	from sklearn.metrics import recall_score, precision_score, f1_score, confusion_matrix, accuracy_score
	# recall
	emotion_recalls = recall_score(all_labels, all_predictions, average=None)
	unweighted_avg_recall = recall_score(all_labels, all_predictions, average='macro')
	# precision
	emotion_precisions = precision_score(all_labels, all_predictions, average=None)
	unweighted_avg_precision = precision_score(all_labels, all_predictions, average='macro')
	# f1
	emotion_f1s = f1_score(all_labels, all_predictions, average=None)
	unweighted_avg_f1 = f1_score(all_labels, all_predictions, average='macro')
	# accuracy
	accuracy = accuracy_score(all_labels, all_predictions)
	# confusion matrix
	confusion_matrix = confusion_matrix(all_labels, all_predictions)

	print 'Unweighted average recall is : %f' % (unweighted_avg_recall)
	print 'Unweighted average precision is : %f' % (unweighted_avg_precision)
	print 'Unweighted average f1 is : %f' % (unweighted_avg_f1)
	print 'accuracy is : %f' % (accuracy)
	print 'Emotion-specific recalls are: '
	print (emotion_recalls)
	print 'Emotion-specific precisions are: '
	print (emotion_precisions)
	print 'Emotion-specific f1 scores are: '
	print (emotion_f1s)
	print 'Confusion matrix for this run is: '
	print (confusion_matrix)

	print 'Entire run completed'
	print '*********************'
	return (emotion_recalls, unweighted_avg_recall, confusion_matrix, accuracy, \
			emotion_precisions, unweighted_avg_precision, emotion_f1s, unweighted_avg_f1)


# Takes a feature vector of variable length and splits it into utterances which are then scored
# against emotions personal models. The predicted label is with highest similarity. True labels
# and predictions are then returned.
def make_predictions_tv(ubm, ivec_machine, whitening_machine, lda_machine, \
			wccn_machine, test_samples, test_labels, frames_per_utterance, norm_type, \
			imp_sqrt_diag_covar_mat, mean_impostor_ivector, emotion_specific_ivectors, \
			debug_level, no_of_emotions):
	predictions=[]; labels = []
	# split test data into short utterances and score them
	num_utt = int(round(len(test_samples)/frames_per_utterance))
	for utt_count in range(0, num_utt):
		t0 = time.time()
		data = test_samples[utt_count*frames_per_utterance \
			: (utt_count+1)*frames_per_utterance]

		utt_ivector = compute_compensated_ivector(data, ubm, ivec_machine, whitening_machine, \
												  lda_machine, wccn_machine)

		# Compute score of projected vector against personal models.
		# And predict emotion according to maximum similarity
		max_similarity = -100000; predicted_label = 0
		for emotion_index in range(no_of_emotions):
			emotion_ivector = emotion_specific_ivectors[emotion_index]
			score_norm = score_vector(utt_ivector, emotion_ivector, norm_type, mean_impostor_ivector, \
									  imp_sqrt_diag_covar_mat)
			if (score_norm > max_similarity):
				max_similarity = score_norm
				predicted_label = emotion_index

		# Calculate the true label for this utterance
		from scipy import stats
		utt_labels = test_labels[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		# get mode (most common value) over all frames in this utterance
		true_label = stats.mode(utt_labels,axis=None)

		labels.append(int(true_label[0][0]))
		predictions.append(int(predicted_label))

		t1 = time.time()
		if debug_level > 1 and utt_count == 0:
			print 'Scoring first test utterance took %fs' % (t1-t0)

	return (predictions, labels)

#Score a given test vector against the personal model and normalize according to norm_type
def score_vector(test_ivector, emotion_ivector, norm_type, mean_impostor_ivector, imp_sqrt_diag_covar_mat):
	if norm_type == 'normcos':
		#see: "Cosine Similarity Scoring without Score Normalization Techniques",
		#      Dehak et al., 2010

		norm_emotion = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(emotion_ivector).T))
		norm_test = np.linalg.norm(np.dot(imp_sqrt_diag_covar_mat, np.atleast_2d(test_ivector).T))

		dist1 = np.subtract(emotion_ivector, mean_impostor_ivector)
		dist2 = np.subtract(test_ivector, mean_impostor_ivector)

		score_norm = np.dot(dist1, dist2) / np.dot(norm_emotion, norm_test)
		# print 'score_norm is %s' % (score_norm)
	else:
		print 'this type of norm (%s) is not supported' % (norm_type)
		exit()

	return score_norm


# Takes a feature vector of variable length and transforms it into a channel-compensated ivector.
def compute_compensated_ivector(samples, ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine):
	#Project test vector (~MAP-adapt UBM)
	gmmstats = bob.machine.GMMStats(ubm.dim_c, ubm.dim_d)
	ubm.acc_statistics(samples, gmmstats)
	projected_ivector = ivec_machine.forward(gmmstats)
	whitened_ivector = whitening_machine.forward(projected_ivector)
	normalized_ivector = whitened_ivector/np.linalg.norm(whitened_ivector)
	lda_ivector = np.ndarray(lda_machine.shape[1], np.float64)
	lda_machine(normalized_ivector, lda_ivector)
	wccn_lda_ivector = np.ndarray(wccn_machine.shape[1], np.float64)
	wccn_machine(lda_ivector, wccn_lda_ivector)

	return wccn_lda_ivector


def get_impostor_emotions_ivectors(ubm, ivec_machine, whitening_machine, lda_machine, wccn_machine, \
	impostor_samples):

	print 'Calculating imposters i-vectors.'
	ivecs = []
	for emotion_specific_files in impostor_samples:
		for file_samples in emotion_specific_files:
			wccn_lda_ivector = compute_compensated_ivector(file_samples, ubm, ivec_machine, \
				whitening_machine, lda_machine, wccn_machine)
			ivecs.append(wccn_lda_ivector)

	print 'imposters i-vectors calculation done.'

	return ivecs


def cosine_distance(a, b):
	if len(a) != len(b):
		raise ValueError, "a and b must be same length"
	numerator = sum(tup[0] * tup[1] for tup in izip(a,b))
	denoma = sum(avalue ** 2 for avalue in a)
	denomb = sum(bvalue ** 2 for bvalue in b)
	result = numerator / (sqrt(denoma)*sqrt(denomb))
	return result


def init_ivector_machine(ivec_machine_model, ubm, ivector_dim, proj_train_features_flat, \
		ir_name, results_path, debug_level, emotion_corpus_name):

	if ivec_machine_model:
		ivec_machine = bob.machine.IVectorMachine(bob.io.HDF5File(ivec_machine_model, 'r'))
		ivec_machine.ubm = ubm
		print 'I-Vector machine loaded.'
	else:
		print 'Starting I-Vector machine training...'
		t0 = time.time()
		ivec_machine = bob.machine.IVectorMachine(ubm, ivector_dim)
		ivec_machine.variance_threshold = 1e-5
		ivec_trainer = bob.trainer.IVectorTrainer(update_sigma=True, max_iterations=10)
		ivec_trainer.train(ivec_machine, proj_train_features_flat)
		print 'I-Vector machine training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		ivec_file = 'ivec_machine_' + str(ivector_dim) + '_' + ir_name +'_'+emotion_corpus_name+ '.hdf5'
		ivec_machine.save(bob.io.HDF5File(results_path + ivec_file, 'w' ))
		print 'I-Vector machine saved to disk.'
	return ivec_machine


def init_whitening_machine(white_machine_model, ivec_machine, proj_train_features_flat, \
	ir_name, results_path, debug_level, emotion_corpus_name):
	if white_machine_model:
		whitening_machine = bob.machine.LinearMachine(bob.io.HDF5File(white_machine_model, 'r'))
		print 'Whitening machine loaded.'
	else:
		print 'Starting whitening enroller training...'
		t0 = time.time()
		ivectors_matrix  = []

		for gmmstats in proj_train_features_flat:
			projected_ivector = ivec_machine.forward(gmmstats)
			ivectors_matrix.append(projected_ivector)

		ivectors_matrix = np.vstack(ivectors_matrix)
		whitening_machine = bob.machine.LinearMachine(ivectors_matrix.shape[1],ivectors_matrix.shape[1])
		t = bob.trainer.WhiteningTrainer()
		t.train(whitening_machine, ivectors_matrix)
		print 'Whitening enroller training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		white_file = 'white_machine_' + ir_name + '_' +emotion_corpus_name+'.hdf5'
		whitening_machine.save(bob.io.HDF5File(results_path + white_file, 'w' ))
		print 'Whitening machine saved to disk.'
	return whitening_machine


def init_lda_machine(lda_machine_model, proj_train_features, lda_subspace_dim, \
	ir_name, results_path, debug_level, emotion_corpus_name):
	if lda_machine_model:
		lda_machine = bob.machine.LinearMachine(bob.io.HDF5File(lda_machine_model, 'r'))
		print 'LDA machine loaded.'
	else:
		print 'Starting LDA projector training...'
		t0 = time.time()
		t = bob.trainer.FisherLDATrainer(strip_to_rank=False, use_pinv=True)
		lda_machine, __eig_vals = t.train(proj_train_features)
		# resize the machine if desired
		lda_machine.resize(lda_machine.shape[0], lda_subspace_dim)
		print 'LDA projector training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		lda_file = 'lda_machine_' + str(lda_subspace_dim) + '_' + ir_name + '_' + emotion_corpus_name+'.hdf5'
		lda_machine.save(bob.io.HDF5File(results_path + lda_file, 'w' ))
		print 'LDA machine saved to disk.'
	return lda_machine


def init_wccn_machine(wccn_machine_model, proj_train_features, ir_name, results_path, debug_level, \
	emotion_corpus_name):
	if wccn_machine_model:
		wccn_machine = bob.machine.LinearMachine(bob.io.HDF5File(wccn_machine_model, 'r'))
		print 'WCCN machine loaded.'
	else:
		print 'Starting WCCN projector training...'
		t0 = time.time()
		t = bob.trainer.WCCNTrainer()
		wccn_machine = t.train(proj_train_features)
		print 'WCCN projector training done.'
		t1 = time.time()
		if debug_level > 0:
			print 'Training took %fs' % (t1-t0)

		wccn_file = 'wccn_machine_' + ir_name +'_'+emotion_corpus_name+ '.hdf5'
		wccn_machine.save(bob.io.HDF5File(results_path + wccn_file, 'w' ))
		print 'WCCN machine saved to disk.'
	return wccn_machine
