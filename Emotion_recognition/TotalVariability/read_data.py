"""
This script reads the datasets
"""
__author__ = 'aihamtaleb'


import glob
import numpy as np
import os
from random import choice
import operator
from sklearn.neighbors import NearestNeighbors
from sklearn import cross_validation

# csv load
def iter_load_csv(filename, delimiter=',', max_samples=3.5E6, dtype=float):
	def iter_func():
		with open(filename, 'r') as infile:
			i = 0
			for line in infile:
				if line.strip():
					line = line.rstrip().split(delimiter)
					if i == max_samples:
						break
					i = i + 1
					for item in line:
						yield dtype(item)
					iter_load_csv.rowlength = len(line)

	data = np.fromiter(iter_func(), dtype=dtype)
	data = data.reshape((-1, iter_load_csv.rowlength))
	return data

def filtr(files, filetype):
	"""Filters a file list by the given filename ending to be accepted"""
	return filter(lambda d: 1 if d.endswith(filetype) else 0, files)

# read ubm and emotion specific datasets
def read_ubm_and_emotions(settings):
	(base_path_features, ubm_corpus_path, emotions_path, ubm_model, test_speaker_path,\
	frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, vad_label_extension,\
	frames_per_utterance, balance_data) = settings

	## emotion models:
	# length of data to be read
	max_length_in_min = 10
	max_length_in_samples = np.int32(np.ceil((max_length_in_min * 60 * sampling_rate) \
									/ (frame_inc * sampling_rate / 1000 )))

	# load emotions data
	all_emotions_folders = os.walk(base_path_features + emotions_path).next()[1]
	all_emotions_folders.sort()

	# actual data
	all_emotions_samples_voiced = []
	all_emotions_samples_voiced_flattened = []
	all_emotions_targets = []
	all_emotions_training_partitions = []

	print 'Reading emotion data for imposters...'
	#iterate over all emotions
	emotion_index = 0
	for emotion in all_emotions_folders:
		# path of samples for emotion
		emotion_feature_path = base_path_features + emotions_path + emotion + '/' \
							   + parameter_file_config_samples + '.csv'
		emotion_labels_path = base_path_features + emotions_path + emotion + '/' \
							   + parameter_file_config_samples + vad_label_extension + '.csv'

		# loading emotion vad samples
		emotion_vad_features = iter_load_csv(emotion_feature_path)
		emotion_samples_voiced = np.array(emotion_vad_features[:,:num_features],'float32')
		emotion_samples_voiced = np.nan_to_num(emotion_samples_voiced)

		emotion_training_labels = iter_load_csv(emotion_labels_path)
		emotion_training_labels = emotion_training_labels.flatten()
		emotion_samples_voiced = emotion_samples_voiced[emotion_training_labels == 1]

		# set the maximum length of each emotion
		current_max_length = min(max_length_in_samples, len(emotion_samples_voiced));
		emotion_samples_voiced = emotion_samples_voiced[:current_max_length,:];
		print 'Reading emotion %d (%s) done.' % (len(all_emotions_samples_voiced), emotion)

		# put training and test data of all emotions into a list
		all_emotions_samples_voiced.append(np.array(emotion_samples_voiced))
		all_emotions_samples_voiced_flattened.extend(np.array(emotion_samples_voiced))
		all_emotions_targets.extend(np.full(len(emotion_samples_voiced), emotion_index))
		emotion_index = emotion_index + 1
	print 'Done reading emotion data for imposters.'

	if balance_data:
		print('Balancing emotion dataset...')
		all_emotions_samples_voiced = \
			balance_dataset(all_emotions_samples_voiced, \
							all_emotions_targets, all_emotions_samples_voiced_flattened)
	# constructing training partitions indices
	for emotion_samples_voiced in all_emotions_samples_voiced:
		cross_indices = []
		# split speaker data into folds for cv; from all voiced samples take last column (label)
		cross_indices[0:k_fold-1] = cross_validation.KFold(len(emotion_samples_voiced), \
			n_folds=k_fold, shuffle=True)
		cross_indices = np.array(cross_indices)
		# create one array containing the training indices of all folds
		cross_training = cross_indices[:,0]
		# put training indices of all emotions into a list
		all_emotions_training_partitions.append(np.array(cross_training))
	if balance_data:
		print('Done balancing emotion dataset.')

	ubm_training_samples_voiced = []
	if not ubm_model:
		print 'Reading UBM data...'
		# ubm paths of data
		ubm_dataset_path = base_path_features + ubm_corpus_path + parameter_file_config_samples + '.csv'
		ubm_labels_path = base_path_features + ubm_corpus_path + parameter_file_config_samples + \
						  vad_label_extension + '.csv'

		# features of vad annotated speech
		ubm_training_features = iter_load_csv(ubm_dataset_path)
		ubm_training_samples_voiced = np.array(ubm_training_features[:, :num_features],'float32')
		ubm_training_samples_voiced = np.nan_to_num(ubm_training_samples_voiced)

		ubm_training_labels = iter_load_csv(ubm_labels_path)
		ubm_training_labels = ubm_training_labels.flatten()
		ubm_training_samples_voiced = ubm_training_samples_voiced[ubm_training_labels == 1]
		print 'Done reading UBM data.'

	if ubm_model:
		print 'Skipping UBM data reading; pre-trained model is used.'

	print 'Reading test speaker emotion samples...'
	## emotion models:
	# length of data to be read
	max_length_in_min = 3
	max_length_in_samples = np.int32(np.ceil((max_length_in_min * 60 * sampling_rate) \
									/ (frame_inc * sampling_rate / 1000 )))

	all_test_speaker_emotions_folders = os.walk(base_path_features + test_speaker_path).next()[1]
	all_test_speaker_emotions_folders.sort()

	all_test_speaker_samples_voiced = []
	all_test_speaker_samples_voiced_flattened = []
	all_test_speaker_targets = []
	all_test_speaker_data_partitions = []

	emotion_index = 0
	for ts_emotion in all_test_speaker_emotions_folders:
		# path of samples for emotion
		ts_emotion_feature_path = base_path_features + test_speaker_path + ts_emotion + '/' \
							   + parameter_file_config_samples + '.csv'
		ts_emotion_labels_path = base_path_features + test_speaker_path + ts_emotion + '/' \
							   + parameter_file_config_samples + vad_label_extension + '.csv'

		# loading emotion vad samples
		ts_emotion_vad_features = iter_load_csv(ts_emotion_feature_path)
		ts_emotion_samples_voiced = ts_emotion_vad_features[:,:num_features]
		ts_emotion_samples_voiced = np.nan_to_num(np.array(ts_emotion_samples_voiced, 'float32'))

		ts_emotion_training_labels = iter_load_csv(ts_emotion_labels_path)
		ts_emotion_training_labels = ts_emotion_training_labels.flatten()
		ts_emotion_samples_voiced = ts_emotion_samples_voiced[ts_emotion_training_labels == 1]

		# set the maximum length of each emotion
		current_max_length = min(max_length_in_samples, len(ts_emotion_samples_voiced))

		ts_emotion_samples_voiced = ts_emotion_samples_voiced[:current_max_length,:]

		print 'Test speaker\'s emotion  %d (%s) done' % (len(all_test_speaker_samples_voiced), ts_emotion)

		all_test_speaker_samples_voiced.append(np.array(ts_emotion_samples_voiced))
		all_test_speaker_samples_voiced_flattened.extend(np.array(ts_emotion_samples_voiced))
		all_test_speaker_targets.extend(np.full(len(ts_emotion_samples_voiced), emotion_index))
		emotion_index = emotion_index + 1
	print 'Done reading test speaker emotion samples.'

	if balance_data:
		print('Balancing test speaker\'s emotion dataset...')
		all_test_speaker_samples_voiced = \
			balance_dataset(all_test_speaker_samples_voiced, \
							all_test_speaker_targets, all_test_speaker_samples_voiced_flattened)
	# constructing testing partitions
	for ts_emotion_samples_voiced in all_test_speaker_samples_voiced:
		cross_indices = []
		# split speaker data into folds for cv; from all voiced samples take last column (label)
		cross_indices[0:k_fold-1] = cross_validation.KFold(len(ts_emotion_samples_voiced), \
			n_folds=k_fold, shuffle=True)
		cross_indices = np.array(cross_indices)
		# create one array containing the training indices of all folds
		cross_testing = cross_indices[:,0]
		# put training indices of all emotions into a list
		all_test_speaker_data_partitions.append(np.array(cross_testing))
	if balance_data:
		print('Done balancing test speaker\'s emotion dataset.')

	return (ubm_training_samples_voiced, all_emotions_samples_voiced, \
			all_test_speaker_samples_voiced, all_emotions_training_partitions, \
			all_test_speaker_data_partitions)


# reading emotion files separately, one at a time
def read_emotions_by_file(settings):
	(base_path_features, emotion_by_file_path, num_features, \
		parameter_file_config_samples, vad_label_extension, frames_per_utterance) = settings

	print 'Reading emotions data by file...'

	#read emotions folders
	all_emotion_folders = os.walk(base_path_features + emotion_by_file_path).next()[1]
	all_emotion_folders.sort()

	all_emotions_files_samples = []

	samples_file_extension = '.csv'
	labels_file_extension = vad_label_extension + '.csv'

	#iterate over all emotions
	for emotion in all_emotion_folders:
		emotion_samples = []

		#get samples for emotion
		emotion_files_path = base_path_features + emotion_by_file_path + emotion + '/*' + samples_file_extension
		emotion_filenames = [f for f in glob.glob(emotion_files_path) if not vad_label_extension in f]
		emotion_filenames.sort()
		for filename in emotion_filenames:
			#get file features
			file_features = iter_load_csv(filename)
			file_samples = np.array(file_features[:,:num_features],'float64')

			#get vad annotation for emotion file
			file_vad_labels_path = filename.replace(samples_file_extension, labels_file_extension)
			if os.path.exists(file_vad_labels_path):
				file_vad_labels = iter_load_csv(file_vad_labels_path)
				file_vad_labels = file_vad_labels.flatten()
				#get all samples that vad predicted as speech (1)
				file_samples_voiced = file_samples[file_vad_labels == 1]

			emotion_samples.append(np.array(file_samples_voiced))

		print 'Emotion %d (%s) done' % (len(all_emotions_files_samples), emotion)
		#put training and test data of all emotions files into a list
		all_emotions_files_samples.append(np.array(emotion_samples))

	print 'Done reading emotions data by file.'
	print 'Number of emotions: %d' % (len(all_emotions_files_samples))

	return all_emotions_files_samples



######
## balance data
######
def balance_dataset(all_samples_voiced, all_targets, all_samples_voiced_flattened):
	counts = np.bincount(np.array(all_targets, 'int'))
	print('Classes distributions before balancing')
	print(counts)
	majority_index, no_majority_samples = max(enumerate(counts), key=operator.itemgetter(1))
	for emotion_index in range(len(all_samples_voiced)):
		if emotion_index == majority_index: continue
		if (counts[emotion_index] > 0.9*float(no_majority_samples)):
			print('skipping emotion %d as it has enough samples.' % emotion_index)
			continue

		print("processing emotion: %d" % emotion_index)
		synthetic_size = np.random.randint( \
			(int(round(0.9*float(no_majority_samples))) - counts[emotion_index]), \
			(no_majority_samples-counts[emotion_index]))
		k = 20
		(synthetic_samples, danger_minorities) = \
			borderlineSMOTE(np.array(all_samples_voiced_flattened,'float32'), \
							all_targets, emotion_index, synthetic_size, k)
		print("done balancing emotion: %d" % emotion_index)

		random_synthetic_indices = np.random.randint(len(synthetic_samples), size=synthetic_size)
		all_samples_voiced[emotion_index] = \
			np.append(all_samples_voiced[emotion_index], \
				  synthetic_samples[random_synthetic_indices], axis=0)

		counts[emotion_index] = len(all_samples_voiced[emotion_index])
	print('Classes distributions after balancing')
	print(counts)

	return all_samples_voiced

def borderlineSMOTE(X, y, minority_target, synthetic_size, k):
	"""
	Returns synthetic minority samples.
	Parameters
	----------
	X : array-like, shape = [n__samples, n_features]: Holds the minority and majority samples
	y : array-like, shape = [n__samples]: Holds the class targets for samples
	minority_target : value for minority class
	synthetic_size : number of new synthetic samples
	k : int. Number of nearest neighbours.
	-------
	synthetic : Synthetic sample of minorities in danger zone
	danger : Minorities of danger zone
	"""
	#Learn nearest neighbours on complete training set
	y = np.array(y,'int8').flatten()

	print("applying SMOTE algorithm on minority samples...")

	#SMOTE danger minority samples
	synthetic_samples = SMOTE(X[y == minority_target], synthetic_size, k, h = 0.5)

	return (synthetic_samples, X[y == minority_target])

def SMOTE(T, synthetic_size, k, h = 1.0):
	"""
	Returns "synthetic_size" synthetic minority samples.
	Parameters
	----------
	T : array-like, shape = [n_minority_samples, n_features]: Holds the minority samples.
	synthetic_size : amount of new synthetic samples: n_synthetic_samples = synthetic_size
	k : int. Number of nearest neighbours.
	Returns
	-------
	S : Synthetic samples. array,
		shape = [synthetic_size, n_features].
	"""
	n_minority_samples, n_features = T.shape
	n_synthetic_samples = synthetic_size
	S = np.zeros(shape=(n_synthetic_samples, n_features))

	#Learn nearest neighbours
	neigh = NearestNeighbors(n_neighbors = k)
	neigh.fit(T)

	#Calculate synthetic samples
	for i in xrange(n_synthetic_samples):
		minority_index = np.random.randint(n_minority_samples)
		nn = neigh.kneighbors(T[minority_index], return_distance=False)
		nn_index = choice(nn[0])
		#NOTE: nn includes T[i], we don't want to select it
		while nn_index == minority_index:
			nn_index = choice(nn[0])
		dif = T[nn_index] - T[minority_index]
		gap = np.random.uniform(low = 0.0, high = h)
		S[i, :] = T[minority_index,:] + gap * dif[:]

	return S
