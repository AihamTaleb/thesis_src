"""
This scripts start the training process of the Gaussian Mixture Models
"""
__author__ = 'aiham'

# can be either 'Semaine' or 'FAU'
emotion_corpus_name = 'Semaine'

# list of testing impulse responses (which smartphones data to use)
testing_ir_names = ['rand_vs_lg_oben','rand_vs_s3_unten','rand_vs_s1_tisch','rand_vs_htc_tasche','none']

if (emotion_corpus_name == 'FAU'):
	# test_speakers = ['all']
	test_speakers = ['Mont_07','Ohm_10','Mont_08','Ohm_18']
else:
	# test_speakers = ['all']
	test_speakers = ['Spike','Obadiah','Prudence','Poppy']

## base results path
base_results_path = '/home/taleb/results/'
## base features path
base_path_features = '/home/taleb/data/corpora_mfcc_dd/'

## create results folder (to store the trained ubm model and evaluation results)
from auxiliary_functions import create_results_folder
results_path = create_results_folder(base_results_path)

######################
### Configurations ###
######################
frame_length = 20
frame_inc = 10
num_features = 36
k_fold = 10
n_jobs_kmeans = 25
num_mixtures_ubm = 128
gmm_covtype = 'diag'
num_iter_em = 100
frames_per_utterance = 256 #256 roughly equals 2.5s
sampling_rate = 16000
balance_data = False

# 0: on CPU
# 1: on GPU
ubm_training_mode = 1

# True: adapt only ubm means
# False: adapt all ubm params
mean_only_adaptation = False

# 0: no additional debug infos
# 1: gpu stats
# 2: detailed scoring; detailed runtimes
debug_level = 2

## filenames
#samples configs
samples_numb_features = 18
parameter_file_config_samples = 'm' + str(samples_numb_features) + 'w' \
	+ str(frame_length) + 'i' + str(frame_inc)

vad_label_extension = '_annoVAD'

# can be either 'voxforge' or 'ubm' --> this refers to TIMIT
ubm_to_use = 'ubm'

#############################
### Training & Evaluation ###
#############################
from gmm_map import gmm_classifier
metrics_list = []
clean_metrics_list = []

for test_speaker in test_speakers:
	print('******* Testing against speaker %s *******' % test_speaker)

	for ir_name in testing_ir_names:
		ubm_model = []
		ubm_corpus_path = ir_name+'/'+ubm_to_use+'/'
		emotions_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/eval/'
		test_speaker_path = ir_name+'/'+emotion_corpus_name+'/all_vs_'+test_speaker+'/test_speaker/'

		settings = (base_path_features, results_path, ubm_model, frame_inc, sampling_rate, \
				k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
				frames_per_utterance, parameter_file_config_samples, vad_label_extension, ubm_corpus_path, \
				emotions_path, test_speaker_path, ir_name, debug_level, ubm_training_mode, \
				emotion_corpus_name, mean_only_adaptation, balance_data)
		result = gmm_classifier(settings)
		if ir_name == 'none':
			clean_metrics_list.append(result)
		else:
			metrics_list.append(result)

	print('******* Done testing against speaker %s *******' % test_speaker)



#####################
## Calculation of evaluation results over clean test data
#####################
from auxiliary_functions import calc_overall_performance
(clean_unweighted_avg_recall, clean_avg_recalls_per_emotions, clean_overall_confusion_matrix, \
	clean_overall_unweighted_avg_precision, clean_avg_precisions_per_emotions, \
	clean_overall_unweighted_avg_f1, clean_avg_f1s_per_emotions, clean_overall_acc) = \
	calc_overall_performance(clean_metrics_list)

print '****************************************************************************************'
print 'Evaluation on %s corpus (No-IRs) is done' % (emotion_corpus_name)
print 'Final unweighted average recall over all emotions (clean) is %.2f' % (clean_unweighted_avg_recall)
print 'Final unweighted average recall for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_recalls_per_emotions]))
print 'Final unweighted average precision over all emotions (clean) %.2f' % (clean_overall_unweighted_avg_precision)
print 'Final unweighted average precision for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_precisions_per_emotions]))
print 'Final unweighted average f1 over all emotions (clean) is %.2f' % (clean_overall_unweighted_avg_f1)
print 'Final unweighted average f1 for all emotions (clean) is %s' % \
	  (', '.join(["%0.2f" % i for i in clean_avg_f1s_per_emotions]))
print 'Final accuracy over all emotions (clean) is %.2f' % (clean_overall_acc)
print 'Final overall confusion matrix over emotions (clean) is : '
print (clean_overall_confusion_matrix)
print '****************************************************************************************'


#####################
## Calculation of evaluation results over noisy (channeled) test data
#####################
(overall_unweighted_avg_recall, avg_recalls_per_emotions, overall_confusion_matrix, \
	overall_unweighted_avg_precision, avg_precisions_per_emotions, overall_unweighted_avg_f1, \
	avg_f1s_per_emotions, overall_acc) = calc_overall_performance(metrics_list)

print '****************************************************************************************'
print 'Evaluation on %s corpus (with-IRs) is done' % (emotion_corpus_name)
print 'Final unweighted average recall over all emotions and sets is %.2f' % (overall_unweighted_avg_recall)
print 'Final unweighted average recall for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_recalls_per_emotions]))
print 'Final unweighted average precision over all emotions and sets is %.2f' % (overall_unweighted_avg_precision)
print 'Final unweighted average precision for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_precisions_per_emotions]))
print 'Final unweighted average f1 over all emotions and sets is %.2f' % (overall_unweighted_avg_f1)
print 'Final unweighted average f1 for all emotions over all sets is %s' % \
	  (', '.join(["%0.2f" % i for i in avg_f1s_per_emotions]))
print 'Final accuracy over all emotions and sets is %.2f' % (overall_acc)
print 'Final overall confusion matrix over all sets and emotions is : '
print (overall_confusion_matrix)
print '****************************************************************************************'