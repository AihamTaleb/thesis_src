__author__ = 'aiham'

### Training
# 1. Trains a Universal Background Model (UBM GMM): using CUDA
# 2. MAP-adapts the UBM for each emotion (= projection)

########################################################################

from gmm_specializer.gmm import GMM
import numpy as np
import time
import bob

def gmm_classifier(settings):
	(base_path_features, results_path, ubm_model, frame_inc, sampling_rate, \
		k_fold, n_jobs_kmeans, num_mixtures_ubm, num_features, gmm_covtype, num_iter_em, \
		frames_per_utterance, parameter_file_config_samples, vad_label_extension, ubm_corpus_path, \
		emotions_path, test_speaker_path, ir_name, debug_level, ubm_training_mode, \
		emotion_corpus_name, mean_only_adaptation, balance_data) = settings

	#################
	### read data ###
	#################
	from read_data import read_ubm_and_emotions
	settings_data = (base_path_features, ubm_corpus_path, emotions_path, ubm_model, test_speaker_path,\
		frame_inc, sampling_rate, k_fold, num_features, parameter_file_config_samples, vad_label_extension,\
		frames_per_utterance, balance_data)
	# ubm, emotion training data, and test data from one speaker.
	(ubm_training_samples_voiced, all_emotions_samples_voiced, \
		all_test_speaker_samples_voiced, all_emotions_training_partitions, \
		all_test_speaker_data_partitions) = read_ubm_and_emotions(settings_data)

	no_of_emotions = len(all_emotions_samples_voiced)
	num_dimension = num_features

	#####################
	### train the UBM ###
	#####################
	training_samples_ubm = np.float32(ubm_training_samples_voiced)
	from auxiliary_functions import train_ubm
	ubm_settings = (num_mixtures_ubm, num_dimension, gmm_covtype, n_jobs_kmeans, training_samples_ubm,
		num_iter_em, debug_level, ir_name, results_path, ubm_training_mode, emotion_corpus_name,
		frames_per_utterance)
	(gmm_fast_ubm, bob_ubm) = train_ubm(ubm_settings)

	ubm_training_samples_voiced = []
	training_samples_ubm = []

	#################################
	#### K-Fold cross validation ####
	#################################
	all_predictions = []
	all_labels = []
	from auxiliary_functions import normalize_features_window
	for cross_index in range(k_fold):
		print('processing fold number %d:' % cross_index)
		### Training personal models (enrollment)
		emotion_personal_gmms = []
		print 'Personal enrollment of emotions...'
		ts_0 = time.time()
		for emotion_index in range(no_of_emotions):
			# build training data emotion
			emotion_training_partitions = all_emotions_training_partitions[emotion_index]
			training_partition_index = emotion_training_partitions[cross_index]
			emotion_samples = all_emotions_samples_voiced[emotion_index]
			if mean_only_adaptation:
				emotion_training_samples = np.float64(emotion_samples[training_partition_index])
				relevance_factor = 0
				trainer = bob.trainer.MAP_GMMTrainer(relevance_factor, True, False, False)
				trainer.convergence_threshold = 1e-4
				# trainer.max_iterations = 50 #int(num_iter_em/2)
				trainer.set_prior_gmm(bob_ubm)
				gmm_emotion = bob.machine.GMMMachine(num_mixtures_ubm,num_dimension)
				trainer.train(gmm_emotion, emotion_training_samples)
				emotion_personal_gmms.append(gmm_emotion)
			else:
				emotion_training_samples = np.float32(emotion_samples[training_partition_index])

				emotion_training_samples = normalize_features_window(emotion_training_samples,
							 len(emotion_training_samples),num_features,frames_per_utterance)

				gmm_fast_emotion = GMM(num_mixtures_ubm, num_dimension, cvtype=gmm_covtype, \
						means=np.array(gmm_fast_ubm.get_all_component_means(), dtype=np.float32), \
						covars=np.array(gmm_fast_ubm.get_all_component_full_covariance(), dtype=np.float32), \
						weights=np.array(gmm_fast_ubm.get_all_component_weights(), dtype=np.float32))
				gmm_fast_emotion.train(emotion_training_samples, max_em_iters=1)
				emotion_personal_gmms.append(gmm_fast_emotion)
		ts_1 = time.time()
		print 'Emotions enrollment done. Time passed: %fs' % (ts_1-ts_0)

		##################
		### Evaluation ###
		##################
		print 'Evaluation of emotions gmms over this fold\'s utterances...'
		from auxiliary_functions import prepare_eval_data
		# preparing test samples and labels
		ts_emotions_samples_voiced_eval = all_test_speaker_samples_voiced
		(test_samples, test_labels) = \
			prepare_eval_data(ts_emotions_samples_voiced_eval, all_test_speaker_data_partitions, \
												   no_of_emotions, cross_index)
		test_samples = np.float32(test_samples)
		test_labels = np.int8(test_labels)

		if (mean_only_adaptation):
			# get predictions and store scores for samples in-place
			(fold_predictions, fold_labels) = make_predictions_gmm_bob(emotion_personal_gmms, \
				test_samples, test_labels, frames_per_utterance, debug_level, no_of_emotions, bob_ubm)
		else:
			# get predictions and store scores for samples in-place
			(fold_predictions, fold_labels) = make_predictions_gmm_fast(emotion_personal_gmms, \
				test_samples, test_labels, frames_per_utterance, debug_level, no_of_emotions, \
				gmm_fast_ubm)

		all_labels.extend(fold_labels)
		all_predictions.extend(fold_predictions)
		print('done processing fold number %d' % cross_index)

	print 'Evaluation of this set is done. Calculating metrics...'
	from sklearn.metrics import recall_score, precision_score, f1_score, confusion_matrix, accuracy_score
	# recall
	emotion_recalls = recall_score(all_labels, all_predictions, average=None)
	unweighted_avg_recall = recall_score(all_labels, all_predictions, average='macro')
	# precision
	emotion_precisions = precision_score(all_labels, all_predictions, average=None)
	unweighted_avg_precision = precision_score(all_labels, all_predictions, average='macro')
	# f1
	emotion_f1s = f1_score(all_labels, all_predictions, average=None)
	unweighted_avg_f1 = f1_score(all_labels, all_predictions, average='macro')
	# accuracy
	accuracy = accuracy_score(all_labels, all_predictions)
	# confusion matrix
	confusion_matrix = confusion_matrix(all_labels, all_predictions)

	print 'Unweighted average recall is : %f' % (unweighted_avg_recall)
	print 'Unweighted average precision is : %f' % (unweighted_avg_precision)
	print 'Unweighted average f1 is : %f' % (unweighted_avg_f1)
	print 'accuracy is : %f' % (accuracy)
	print 'Emotion-specific recalls are: '
	print (emotion_recalls)
	print 'Emotion-specific precisions are: '
	print (emotion_precisions)
	print 'Emotion-specific f1 scores are: '
	print (emotion_f1s)
	print 'Confusion matrix for this run is: '
	print (confusion_matrix)

	print 'Entire run completed'
	print '*********************'
	return (emotion_recalls, unweighted_avg_recall, confusion_matrix, accuracy, \
			emotion_precisions, unweighted_avg_precision, emotion_f1s, unweighted_avg_f1)

# Takes a feature vector of variable length and splits it into utterances which are then scored
# against emotions personal models. The predicted label is with highest similarity. True labels
# and predictions are then returned.
def make_predictions_gmm_fast(emotion_personal_gmms, test_samples, test_labels, \
			 frames_per_utterance, debug_level, no_of_emotions, gmm_fast_ubm):

	from auxiliary_functions import norm_utt
	fold_predictions=[]; fold_labels = []
	# split test data into short utterances and score them
	num_utt = int(round(len(test_samples)/frames_per_utterance))
	for utt_count in range(0, num_utt):
		data = test_samples[utt_count*frames_per_utterance \
			: (utt_count+1)*frames_per_utterance]

		data = norm_utt(data)

		# Compute score of projected vector against personal models.
		# And predict emotion according to maximum similarity
		max_score = -100000; predicted_label = 0
		for emotion_index in range(no_of_emotions):
			emotion_gmm = emotion_personal_gmms[emotion_index]
			mean_log_lklds_fast_emotion = np.mean(emotion_gmm.score(data))
			mean_log_lklds_fast_ubm = np.mean(gmm_fast_ubm.score(data))
			score = mean_log_lklds_fast_emotion - mean_log_lklds_fast_ubm
			if (score > max_score):
				max_score = score
				predicted_label = emotion_index

		# Calculate the true label for this utterance
		utt_labels = test_labels[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		# get mode (most common value) over all frames in this utterance
		true_label = round(np.mean(utt_labels))

		fold_labels.append(int(true_label))
		fold_predictions.append(int(predicted_label))

	return (fold_predictions, fold_labels)



# Takes a feature vector of variable length and splits it into utterances which are then scored
# against emotions personal models. The predicted label is with highest similarity. True labels
# and predictions are then returned.
def make_predictions_gmm_bob(emotion_personal_gmms, test_samples, test_labels, \
			 frames_per_utterance, debug_level, no_of_emotions, bob_ubm):

	fold_predictions=[]; fold_labels = []
	frames_emotions = []
	for frame_index in range(len(test_samples)):
		frame = test_samples[frame_index]
		# predict emotion according to maximum log likelihood
		max_score = -100000; predicted_label = 0
		for emotion_index in range(no_of_emotions):
			emotion_gmm = emotion_personal_gmms[emotion_index]
			log_lkld_emotion = emotion_gmm(np.array(frame,'float64'))
			log_lkld_ubm = bob_ubm(np.array(frame,'float64'))
			score = log_lkld_emotion - log_lkld_ubm
			if (score > max_score):
				max_score = score
				predicted_label = emotion_index
		frames_emotions.append(predicted_label)

	num_utt = int(round(len(frames_emotions)/frames_per_utterance))
	for utt_count in range(0, num_utt):
		# Calculate the labels for this utterance
		utt_true_labels = test_labels[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		true_label = round(np.mean(utt_true_labels))
		fold_labels.append(int(true_label))

		utt_pred_labels = frames_emotions[utt_count*frames_per_utterance : (utt_count+1)*frames_per_utterance]
		predicted_label = round(np.mean(utt_pred_labels))
		fold_predictions.append(int(predicted_label))

	return (fold_predictions, fold_labels)